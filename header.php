<?php
//Home page of site
echo '<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>pressfa</title>
  <link rel="stylesheet" type="text/css" href="themes/default/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="themes/default/css/bootstrap-rtl.min.css">
  <link rel="stylesheet" type="text/css" href="themes/default/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="themes/default/css/style.css">
  <script src="themes/default/js/jquery.min.js"></script>
  <script src="themes/default/js/bootstrap.min.js"></script>
  <script src="themes/default/js/all.min.js"></script>
  <script src="themes/default/js/script.js"></script>
</head>
<body dir="rtl" class="bg-info">
<div id="o_container" class="container bg-white">
  <header class="blog-header py-3 navbar-dark bg-dark">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div id="user_welcome" class="col-4 pt-1">
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-white" href="#">پرسفا</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
        <a id="search_link" class="text-muted" href="#" aria-label="Search">
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Search</title><circle cx="10.5" cy="10.5" r="7.5"/><path d="M21 21l-5.2-5.2"/></svg>
        </a>
        <a id="login_link" class="btn btn-sm btn-outline-secondary" href="login.php">ورود</a>
        <a id="signup_link" class="btn btn-sm btn-outline-secondary" href="#">ثبت نام</a>
      </div>
    </div>
  </header>

  <div class="nav-scroller py-1 mb-2 bg-white text-dark">
    <nav class="nav d-flex justify-content-between">
      <a class="p-2 text-muted" href="#">جهان</a>
      <a class="p-2 text-muted" href="#">ایران</a>
      <a class="p-2 text-muted" href="#">تکنولوژی</a>
      <a class="p-2 text-muted" href="#">طراحی</a>
      <a class="p-2 text-muted" href="#">فرهنگ</a>
      <a class="p-2 text-muted" href="#">تجارت</a>
      <a class="p-2 text-muted" href="#">سیاست</a>
      <a class="p-2 text-muted" href="#">علمی</a>
      <a class="p-2 text-muted" href="#">سلامتی</a>
      <a class="p-2 text-muted" href="#">سبک</a>
      <a class="p-2 text-muted" href="#">سفر</a>
    </nav>
  </div>

  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div style="height:500px;" class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="themes/default/images/sliderimages/1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="themes/default/images/sliderimages/2.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="themes/default/images/sliderimages/3.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

  <div class="row mb-2">
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">علمی</strong>
          <h3 class="mb-0">عنوان خبر</h3>
          <div class="mb-1 text-muted">13 شهریور</div>
          <p class="card-text mb-auto">Lorem ipsum dolor  consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore </p>
          <a href="#" class="stretched-link">در پرسفا بخوانید</a>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="200px" src="themes/default/images/sceince.jpg">
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary">تکنولوژی</strong>
          <h4 class="mb-0">لوگو جدید اینتل رونمایی شد</h4>
          <div class="mb-1 text-muted">13 شهریور</div>
          <p class="mb-auto">اینتل ضمن معرفی پردازنده های نسل  یازدهمی تایگر لیک، اعلام کرد لوگو خود را نیز به روزرسانی کرده است.</p>
          <a href="#" class="stretched-link">در پرسفا بخوانید</a>
        </div>
        <div class="col-auto d-none d-lg-block">
          <img width="250px" height="200px" src="themes/default/images/intel_logo.jpg">
        </div>
      </div>
    </div>
  </div>
<div class="row">
    <div class="col-md-8 blog-main">
      <h3 class="pb-4 mb-4 font-italic border-bottom">
        داغ ترین اخبار
      </h3>

      <div class="blog-post">
        <h2 class="blog-post-title">پابجی موبایل و 118 اپلیکیشن چینی در هند مسدود شدند.</h2>
        <p class="blog-post-meta">13 شهریور 1399<br><i class="fas fa-pen-alt"></i><a href="#">نویسنده: مصطفی نوری</a></p>

        <p></p>
        <hr>
        <p>دولت هند رسما اعلام کرد که 118 اپلیکیشن چینی دیگر را مسدود کرده است که در بین آن‌ها، بازی بسیار محبوب پابجی موبایل(PUBG Mobile) هم به چشم میخورد.</p>
        <p>وزارت الکترونیک فناوری اطلاعات هند در اطلاعیه ای رسمی اعلام کرد دلیل اصلی مسدود شدن اپلیکیشن‌های چینی این است درگیرفعالیت‌هایی می‌شوند که برای حق حاکمیت و تمامیت هند زیان آورهستند.</p>
      </div><!-- /.blog-post -->

      <div class="blog-post">
        <h2 class="blog-post-title">روسیه اپل و گوگل را به کاهش کارمزد اپ استور و پلی استور مجبورمی‌کند؟</h2>
        <p class="blog-post-meta">13 شهریور 1399<br><i class="fas fa-pen-alt"></i><a href="#">نویسنده: مصطفی نوری</a></p>
        <p>اپل و گوگل، صاحبان فروشگاه‌های آنلاین اپ‌ استور و پلی استور، درچندوقت اخیر با فشار فراوان کاربران وشرکت‌های مختلف روبه‌رو شده اند. دلیل اصلی انتقاد از اپل و گوگل سیاست آن‌ها برای دریافت کارمزد به‌ازای خریداپلیکیشن‌ها است.</p>
      </div><!-- /.blog-post -->
      <nav class="blog-pagination">
        <a class="btn btn-outline-primary" href="#">قدیمی تر</a>
        <a class="btn btn-outline-secondary disabled" href="#" tabindex="-1" aria-disabled="true">جدیدتر</a>
      </nav>

    </div><!-- /.blog-main -->

    <aside class="col-md-4 blog-sidebar">
      <div class="p-4 mb-3 bg-light rounded">
        <h4 class="font-italic">درباره پرسفا</h4>
        <p class="mb-0">پرسفا یک سیستم مدیریت محتوای رایگان ومنبع باز است که میتوان از آن  برای نشراخبار، انجمن‌های گفتگو، سایت‌های عضویت، سیستم‌های مدیریت یادگیری وفروشگاه‌های آنلاین کمک گرفت.</p>
      </div>

      <div class="p-4">
        <h4 class="font-italic">آرشیو اخبار</h4>
        <ol class="list-unstyled mb-0">
          <li><a href="#">شهریور 1399</a></li>
          <li><a href="#">مرداد 1399</a></li>
          <li><a href="#">تیر 1399</a></li>
          <li><a href="#">خرداد 1399</a></li>
          <li><a href="#">ادریبهشت 1399</a></li>
          <li><a href="#">فروردین 1399</a></li>
          <li><a href="#">اسفند 1398</a></li>
          <li><a href="#">بهمن 1398</a></li>
          <li><a href="#">دی 1398</a></li>
          <li><a href="#">آذر 1398</a></li>
          <li><a href="#">آبان 1398</a></li>
          <li><a href="#">مهر 1398</a></li>
        </ol>
      </div>

      <div class="p-4">
        <h4 class="font-italic">سایر رسانه های پرسفا</h4>
        <ol class="list-unstyled">
          <li><a href="#">توییتر</a></li>
          <li><a href="#">تلگرام</a></li>
          <li><a href="#">اینستاگرام</a></li>
        </ol>
      </div>
    </aside><!-- /.blog-sidebar -->
  </div><!-- /.row --><p>
    <a href="#"><i class="fas fa-arrow-alt-circle-up" style="font-size:24px;color:red"></i></a>
  </p>
  </div>';
  //Check Admin Login
  if($isLoggedIn === 1)
  {
    ?>
    <script>
    $("#user_welcome").html('<span class="text-muted">سلام Admin:</span>');
    $("#login_link").remove();
    $("#signup_link").remove();
    $("#search_link").after('<a id="logout_link" class="btn btn-sm btn-outline-secondary" href="logout.php">خروج</a>')
    $("#logout_link").after('<a id="dashboard_link" class="btn btn-sm btn-outline-secondary" href="admin">پیشخوان</a>')
    </script>
  <?php
  }
?>