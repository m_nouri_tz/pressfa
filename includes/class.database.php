<?php
//DB class for work with database
class DB
{
    //Function for Connect to DB
	public static function connect()
    {
        global $dbhost,$dbname,$dbuname,$dbpass;// parameters require for connect to database
        
        try{
            return new PDO("mysql:host=$dbhost;dbname=$dbname;",$dbuname,$dbpass,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));//-->PDO Connection String For Connect To DB
        }
        catch(PDOException $e){
            return 0;
        }
    }
    //Function For Add/Update Record
    public static function AddRecord(&$params,$tableName)
    {
        global $prefix,$dev,$link;
        $paramsKeys = array_keys($params);//get parameters keys
        $paramsValues = array();//get parameters Values
        foreach ($paramsKeys as $key)
            $paramsValues[]=":".$key;
            /*
                For Example: $params(pid=1,ptitle=test,pbody=test2)
                $paramsKeys = (pid,ptitle,pbody)
                In Foreach Loop
                {
                    $key=pid $paramsValues[0]=:pid//1 time
                    $key=ptitle $paramsValues[1]=:ptitle//2 time
                    $key=pbody $paramsValues[1]=:pbody//3 time
                }
            */
        // INSERT query (Add)
        $q="INSERT INTO ".$prefix.$tableName."
            (".implode(",", $paramsKeys).")
        VALUES 
            (".implode(",", $paramsValues).")
        ";
        $query = $link->prepare($q);// safe query with prepare() function
        foreach ($params as $key => $value)
        {
            if($value==="")
                $value=null;
            $query->bindValue(":".$key,$value);
        }
        /*
        For Ex:
        INSERT INTO ppp_posts
            (uid,ptitle,pbody,puri,padd_date,pimage,pcomments)
        VALUES 
            (:uid(1),:ptitle(Test title),:pbody(Test body),:puri(Test uri),:padd_date(Test adddate),:pimage(Test pimage),:pcomments(Test comments))
        */
        $query->execute();
        if($dev==1)//developer mode for debug
        {
            echo '<pre style="direction: ltr; text-align: left">params';
            print_r($params);//print all parameters passed to AddRecord function
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            echo $q;//print query
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">errorInfo';
            print_r($query->errorInfo());//print all errors in mysql
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">debugDumpParams';
            print_r($query->debugDumpParams());//print sent parameters list in query
            echo '</pre>';
        }
        $counts = $query->rowCount();//get row count for mean added record
        if($counts!=0)
            return $link->lastInsertId();
        return 0;
    }
    //Function for ListRecords
    //where mtitle like '%:mtitle%' AND maddress like '%:madress%'
	public static function ListRecords($columns,$tableName,$where='',$whereArray=array(),$order='',$limit='')//-->Create ListRecords Static Method
    {
        global $prefix,$dev,$link;
        $q = "SELECT $columns FROM ".$prefix.$tableName." $where $order $limit";// SELECT query (List)
        $query = $link->prepare($q);// safe query with prepare() function
        foreach($whereArray as $key => $value) 
            $query->bindValue(":".$key,$value);
        $query->execute();
        if($dev==1)
        {
            echo '<pre style="direction: ltr; text-align: left">';
            echo $q;//print query
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($query->errorInfo());//print all errors in mysql
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($query->debugDumpParams());//print sent parameters list in query
            echo '</pre>';
        }
        $result = $query->fetchAll(PDO::FETCH_ASSOC);//get data of database(base columns name)
        return $result;
    }
    //Function for DeleteRecords
    public static function DeleteRecords($ids,$columnName,$tableName)
    {
        global $prefix,$dev,$link;
        $q = "DELETE FROM ".$prefix.$tableName." WHERE $columnName IN(".implode(",", $ids).")";// DELETE query
        $query = $link->prepare($q);// safe query with prepare() function
        $query->execute();
        if($dev==1)
        {
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($params);//print all parameters passed to DeleteRecords function
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            echo $q;//print query
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($query->errorInfo());//print all errors in mysql
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($query->debugDumpParams());//print sent parameters list in query
            echo '</pre>';
        }
        $counts = $query->rowCount();//get row count for mean deleted record
        return $counts;
    }
    //Function For UpdateRecord
    public static function UpdateRecord($tableName,&$params,$where)
    {
        global $prefix,$dev,$link;
        $paramsKeys = array_keys($params);//get parameters keys
        $setValues = "";
        $c = 0;//counter varaible
        $arrayCount = count($paramsKeys);
        foreach ($paramsKeys as $key)
        {
            $setValues.=$key."=:".$key.(($c == $arrayCount-1)?'':', ');// put , end of index of array except last index
            $c++;
        }
        $q = "UPDATE ".$prefix.$tableName." SET $setValues $where";// UPDATE query
        $query = $link->prepare($q);// safe query with prepare() function

        foreach ($params as $key => $value)
        {
            if($value==="")
                $value=null;
            $query->bindValue(":".$key,$value);
        }
        $query->execute();
        if($dev==1)
        {
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($params);//print all parameters passed to UpdateRecord function
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            echo $q;//print query
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($query->errorInfo());//print all errors in mysql
            echo '</pre>';
            echo '<pre style="direction: ltr; text-align: left">';
            print_r($query->debugDumpParams());//print sent parameters list in query
            echo '</pre>';
        }
        $res = $query->errorInfo();
        if($res[0] == 0)
            return true;
        else
            return flase;
    }
}
//Connect To DB
$link = DB::connect();
?>