<div id="quick_edit_form" class="d-flex align-content-center flex-wrap ">
      	   <div style="width:40%;">
      	      <h5><span class="badge badge-info float-right">فرم ویرایش سریع</span></h5><br>
      	      <form id="add_post_form" action = "" method="post" enctype="multipart/form-data"><br>
      	         <div class="form-group">
      	            <label for="ptitle">عنوان مطلب:&nbsp&nbsp</label>
      	            <input type="text" class="form-control" name="ptitle" id="ptitle" value="'.$recordInfo['ptitle'].'" autofocus>
      	            <div id="ptitle_result"></div>
      	         </div><br>
      	         <div class="form-group">
      	            <label for="puri">نامک مطلب:&nbsp&nbsp&nbsp</label>
      	            <input type="text" class="form-control" name="puri" id="puri" dir="ltr" value="'.$recordInfo['puri'].'">
      	            <div id="puri_result"></div>
      	            <div id="puri_Rules" class="text-danger">موارد مجاز شامل حروف لاتین، اعداد، خط تیره می باشد.</div>
      	         </div><br>
      	         <div class="form-group">
      	            <label id="pvisible" for="pvisible">وضعیت مشاهده مطلب </label>
      	            <div class="form-group" id="pvisible_items" >
      	               '.$recordInfo['ppublic'].'
      	            </div>
      	         </div>
      	         <div class="form-group">
      	          <label id="comment_status_label" for="comment_status_label">وضعیت دیدگاه مطلب<i class="fa fa-sort-down"></i></label>
      	          <div id="comment_status_items">
      	            '.$recordInfo['pcomment_status'].'
      	         </div>
      	      </div>
      	      <div class="form-group">
      	       <label id="post_publish_date_label" for="post_publish_date_label"> زمان انتشار مطلب<i class="fa fa-sort-down"></i></label>
      	       <div id="post_publish_date_items">
      	         <button id="ppublish_date" name="ppublish_date" type="button" class="btn btn-primary btn-sm">'.Englishtopersian(G2J($recordInfo['ppublish_date'],$lang)).'</button> <input  id="ppublish_date_now" name="ppublish_date_now"  type="hidden" class="btn btn-secondary btn-sm" value="پس از درج مطلب"> <div id="date_error"></div>
      	      </div>
      	   </div>
      	   <div class="form-group">
      	   <label id="post_category_label" for="post_category_label">دسته ها<i class="fa fa-sort-down"></i></label><br>
      	   <div id="cats_form" class="form-group ">
      	    <label id="cat_search_label" for="cat_search">جستجوی دسته‌ها:</label><br>
      	    <input class="form-control" type="text" id="cat_search" name="cat_search"><br>
      	    <span id="cats_result"></span><br>
      	    <span id="error_load_cat"></span>
      	    <div id="post_category_items" class="form-group" class="form-control">
      	    </div>
      	 </div>
      	</div>
      	</form>
      	</div>
      	</div>