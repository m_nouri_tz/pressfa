<?php
//default==>2 or more space =>1 space 
//or
//everey character replace by space
function Trimes($string,$charOld='/ * /',$charNew = " ")
{
	return preg_replace($charOld, $charNew, $string);
}
function Englishtopersian(&$input) 
{
  $inputstring = [];
  $inputstring = $input;
  $persian = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  $english = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  for($j = 0;$j<count($inputstring);$j++)
  {
    for ($i = 0; $i < 10; $i++) 
    {
        $inputstring = str_replace($english[$i], $persian[$i],$inputstring);
    }
  }
  return $inputstring;
}
//Success message
function Success($message,$print = 1)
{
	$message = '<div class="alert alert-success">'.$message.'...</div>';
	if($print == 1)
		echo $message;
	else
		return $message;
}
//Error message
function Error($message,$print = 1)
{
	$message = '<div class="alert alert-danger">'.$message.' !</div>';
	if($print == 1)
		echo $message;
	else
		return $message;
}
function ListPosts($admin,$print=1)
{
	global $pageLimit,$lang,$prefix;
	$content = "";//. replace echo
	$start = 0;//start pageing 
	$pageFilter = 1;//first page_filter number
	$q = "";// varaible query
	$where = "";
	$where2 = "";
	$where5 = "";
	$where6 = "";
	$searchFilter = "";// varaible search
	$whereArray = array();
	$whereArray5 = array();
	$key = "pid";
	$order = "desc";//because database is asc
	$comboDate = [];
	$dateFilter = "همه تاریخ ها";
	$dateKeeper = "";
	$comboCat = [];
	$catFilter = "همه دسته ها";
	$catKeeper = "";
	//Check Posts Filter section
	if(isset($_POST['search']))
	{
		$pageLimit = $_POST['page_limit'];
		$pageFilter = (isset($_POST['page_filter'])?$_POST['page_filter']:1);// if user not set page_filter => 1
		$start = ($pageFilter-1)*$pageLimit;//formula paging
		if(!empty($_POST['q']))//If the user wanted to search
		{
			$q = $_POST['q'];
			$searchFilter = $_POST['search_filter'];
			$where = "WHERE $searchFilter LIKE :$searchFilter";// like
			$whereArray = array($searchFilter=>"%$q%");
		}
		$key = $_POST['key'];
		$order = $_POST['order'];
		if(!empty($_POST['date_keeper']))//If the user wanted to filter the date
		{
			$dateKeeper = $_POST['date_keeper'];//1399-10-12 00:00:00
			$dateKeepersplit = [];
			$dateKeepersplit = explode(" ", $dateKeeper);//1399-10-12
			$dateKeepersplit = explode("-", $dateKeepersplit[0]);//1399-10-12
			$monthName = [
				"فروردین" =>"1",
				"اردیبهشت"=>"2",
				"خرداد"=>"3",
				"تیر"=>"4",
				"مرداد"=>"5",
				"شهریور"=>"6",
				"مهر"=>"7",
				"آبان" =>"8",
				"آذر" =>"9",
				"دی"=>"10",
				"بهمن"=>"11",
				"اسفند"=>"12"	
			];
			$dateKeepersplit[1] = array_search($dateKeepersplit[1],$monthName);// convert monthName to monthNumber"دی"
			$inputstring = [];
			$inputstring = $dateKeepersplit[0];// keep date Ex:1398 => ۱۳۹۸
			$persian = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
			$english = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
			for($j = 0;$j<count($inputstring);$j++)
			{
			  for ($i = 0; $i < 10; $i++) 
			  {
			      $inputstring = str_replace($english[$i], $persian[$i],$inputstring);
			  }
			}
			$dateFilter = $_POST['date_filter'];
			$where = "WHERE padd_date LIKE :padd_date";
			$b = J2G($dateKeeper,$lang='english',$i="date_filter");
			$whereArray = array($dateFilter=>"%$b%");
			$dateFilter = "همه تاریخ ها";
		}
	}
	$order_string = "ORDER BY $key $order";
	$where = "INNER JOIN ".$prefix."users ON ".$prefix."users.uid=".$prefix."posts.uid ".$where;//which user added post
	$records = DB::ListRecords('*','posts',$where,$whereArray,$order_string,"limit $start,$pageLimit");	
	$where2 = "INNER JOIN ".$prefix."category_post ON ".$prefix."category_post.cid=".$prefix."categories.cid".$where2;
	$records2 = DB::ListRecords('*','categories',$where2,$whereArray2);
	$where5 = "INNER JOIN ".$prefix."category_post ON ".$prefix."category_post.cid=".$prefix."categories.cid ";//which user added post
	$records5 = DB::ListRecords('*','categories',$where5);
	if(!empty($_POST['cat_keeper']))//If the user wanted to filter the categories
	{
		$catKeeper = $_POST['cat_keeper'];
		$catFilter = $_POST['cat_filter'];
		$where6 = "WHERE $catFilter LIKE :$catFilter";
		$whereArray6 = array($catFilter=>"%$catKeeper%");
		$catFilter = "همه دسته ها";
		$where6 = $where5.$where6;//which user added post
		$records6 = DB::ListRecords('*','categories',$where6,$whereArray6);
		foreach ($records6 as $recordInfo6)// combo box catFilter
		{
			$where7[] = $recordInfo6['pid'];
		}
		$where =$where." AND ".$prefix."posts.pid IN(".implode(",", $where7).")";
		$records = DB::ListRecords('*','posts',$where,$whereArray,$order_string,"limit $start,$pageLimit");
	}
	$where3 = "INNER JOIN ".$prefix."comments ON ".$prefix."comments.pid=".$prefix."posts.pid".$where3;//which user added post
	$record3 = DB::ListRecords('*','posts',$where3,$order_string);
	$records4 = DB::ListRecords('padd_date','posts',$order_string);
	$recordsCount = DB::ListRecords('count(*) as count','posts',$where,$whereArray);// for paging
	$recordsCount = $recordsCount[0]['count'];// get number only
	$search_culomns = array(//  user want search culomns
		"ptitle"=>'عنوان مطلب',
		"puri"=>'نامک مطلب',
		"pbody"=>'متن مطلب'
	);
	//Posts Filter Form
	$content.= '<form action="" method="post" class="form-inline justify-content-left" id="filter_form">';
	$content.= '<div>';
	$content.= 'جستجو: <input type="text" name="q" id="q"  value="'.$q.'" class="form-control">';
	$content.= '<select name="search_filter" id="search_filter" class="form-control">';
	foreach ($search_culomns as $column_id=>$column_name)// combo box search
	{
		$content.='
		<option value="'.$column_id.'"'.($searchFilter == $column_id ?'selected':'').'>'.$column_name.'</option>';// if user search combo box selected
	}
	$content.= '</select>';
	$content.= '
	&nbsp;تعداد رکورد در صفحه:
	<input type="text" name="page_limit" id="page_limit" value="'.$pageLimit.'" class = "form-control col col-lg-1">
	';
	if($recordsCount>$pageLimit)// understand paging require or no
	{
		$content.= 'صفحه: <select name="page_filter" id="page_filter" class="form-control ltr">';
		for($i = 1;$i <= ceil($recordsCount/$pageLimit);$i++)// ceil (number of records / page_limit )=>5/2=3
		{
			$content.= '<option value="'.$i.'" '.($i == $pageFilter?'selected':'').'>'.$i.'</option>';
		}
		$content.= '</select>';
	}
	$content.= '&nbsp;<select name="date_filter" id="date_filter" class="form-control">';
	foreach ($records4 as $recordInfo4)// combo box dateFilter
	{
		if(!in_array($dateFilter,$comboDate))
		{
			array_push($comboDate,$dateFilter);
		}
		if(!in_array(G2J($recordInfo4['padd_date'],$lang='fa',$i="date_filter"),$comboDate))
		{
			array_push($comboDate,G2J($recordInfo4['padd_date'],$lang='fa',$i="date_filter"));//G2J($recordInfo['padd_date'],$lang,$i="date_filter")
		}
	}
	$t = count($comboDate);
	for ($a=0; $a <= $t-1; $a++) 
	{
		$content.='
		<option   value="padd_date" '.($comboDate[$a] == $dateKeepersplit[1]." ".$inputstring?'selected':'').'>'.$comboDate[$a].'</option>';//'.($dateFilter == 'padd_date'?'selected':'').' if user dateFilter combo box selected
	}
	$content.= '</select>';
	$content.= '<input type="hidden" name="date_keeper" id="date_keeper" value="'.$dateKeeper.'">';
	
	$content.= '&nbsp;<select name="cat_filter" id="cat_filter" class="form-control">';
	foreach ($records5 as $recordInfo5)// combo box catFilter
	{
		if(!in_array($catFilter,$comboCat))
		{
			array_push($comboCat,$catFilter);
		}
		if(!in_array($recordInfo5['ctitle'],$comboCat))
		{
			array_push($comboCat,$recordInfo5['ctitle']);
		}
	}
	$t5 = count($comboCat);
	for ($a5=0; $a5 <= $t5-1; $a5++) 
	{
		$content.='
		<option value="ctitle" '.($comboCat[$a5] == $catKeeper?'selected':'').'>'.$comboCat[$a5].'</option>';//'.($dateFilter == 'padd_date'?'selected':'').' if user dateFilter combo box selected
	}
	$content.= '</select>&nbsp;';
	$content.= '<input type="text" hidden name="cat_keeper" id="cat_keeper" value="'.$catKeeper.'">';

	$content.= '<input type="hidden" name="key" id="key" value="'.$key.'">';
	$content.= '<input type="hidden" name="order" id="order" value="'.$order.'">';
	$content.= '<input type="submit" name="search" id="search" value="برو" class="btn btn-primary btn-sm">';
	$content.= '</div>';
	$content.= '<a name="multipledelete" style="margin-top:5px;" id="multipledelete"  class="badge badge-danger confirm" href="?op=del&multipledelete">حذف دسته جمعی</a>';
	$content.= '<br><span id="delete_error"></span>';
	$content.= '</form>
	<script>
	$("#page_limit,#q").on("change",function(){
		$("#page_filter").val("1");
	})
	$(function(){
		$("select[name=date_filter] option").on("click",function(){
			var a = $("select[name=date_filter] option").filter(":selected").text();
			if(a == "padd_date" || a == "همه تاریخ ها")
			{
				a = a.replace(a,"همه تاریخ ها");
				a = "";
				$("#date_keeper").val("");
			}
			a = a.split(" ");
			var monthName = [
				"فروردین", 
				"اردیبهشت", 
				"خرداد", 
				"تیر", 
				"مرداد", 
				"شهریور", 
				"مهر", 
				"آبان", 
				"آذر", 
				"دی",
				"بهمن",
				"اسفند"
			];
			for(var i=0;i <= monthName.length-1;i++) 
			{ 
				var monthNum = monthName.indexOf(a[0])+1;// convert monthName to monthNumber
			}
			var inputstring = [];
			inputstring = a[1];// keep date Ex:۱۳۹۸
			inputstring = inputstring.split("");
			var persian = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
			for(var j = 0; j <= inputstring.length-1;j++)
			{
				for (var i = 0; i < 10; i++) 
				{
					var f = [4];// For keep date number
					f[0] = persian.indexOf(inputstring[0]);
					f[1] = persian.indexOf(inputstring[1]);
					f[2] = persian.indexOf(inputstring[2]);
					f[3] = persian.indexOf(inputstring[3]);
				}
			}
			f[0] = f[0].toString();
			f[1] = f[1].toString();
			f[2] = f[2].toString();
			f[3] = f[3].toString();
			$("#date_keeper").val(f[0]+f[1]+f[2]+f[3]+"-"+monthNum+"-"+"12"+" "+"00"+":"+"00"+":"+"00");//Fill in the date keeper value with the date selected by the user(dateFilter combobox)
		})

		$("select[name=cat_filter] option").on("click",function(){
			var h = $("select[name=cat_filter] option").filter(":selected").text();
			if(h == "همه دسته ها")
			{
			// 	h = h.replace(h,"همه دسته ها");
				h = "";
				$("#cat_keeper").val("");
			}
			$("#cat_keeper").val(h);
		})
		
		$(".sort").on("click",function(){
			$("#key").val($(this).attr("key"));
			$("#order").val($(this).attr("order"));
			$("#filter_form #search").click();
		})
		$(".confirm").on("click",function(){
			if(confirm("آیا از انجام این عملیات اطمینان دارید؟"))
				return true;
			else
				return false;
		})
	})
	</script>
	';
	// show posts list(paging/search/sort) for endusers
	$content.='
	<div class="table-responsive bg-white">
		<form action="" method="post">
		<table id="list_table" class="table table-bordered" style="text-align:center;">
			<tr>
				<th style="text-align: center"> همه<br><input type="checkbox" class="select_all"></th>
				<th style="text-align: center"><a href="#" key=ptitle order='.($order=="asc"?'desc':'asc').' class="sort">عنوان '.($key == "ptitle"?'<i class="fas fa-sort-'.($order == 'asc'?'up':'down').'"></i>':'').'</a></th>
				<th style="text-align: center"><a href="#" key=puri order='.($order=="asc"?'desc':'asc').' class="sort">نامک '.($key == "puri"?'<i class="fa fa-sort-'.($order == 'asc'?'up':'down').'"></i>':'').'<a></th>
				<th style="text-align: center"><a href="#" key=padd_date order='.($order=="asc"?'desc':'asc').' class="sort">تاریخ درج '.($key == "padd_date"?'<i class="fa fa-sort-'.($order == 'asc'?'up':'down').'"></i>':'').'</a></th>
				<th style="text-align: center"><a href="#" key=pmod_date order='.($order=="asc"?'desc':'asc').' class="sort">تاریخ آخرین ویرایش'.($key == "pmod_date"?'<i class="fa fa-sort-'.($order == 'asc'?'up':'down').'"></i>':'').'</a></th>
				<th style="text-align: center">دسته(ها)</th>
				<th style="text-align: center"><a href="#" key=coauthor_name order='.($order=="asc"?'desc':'asc').' class="sort">دیدگاه(ها)'.($key == "coauthor_name"?'<i class="fa fa-sort-'.($order == 'asc'?'up':'down').'"></i>':'').'</a></th>
				<th style="text-align: center"><a href="#" key=uusername order='.($order=="asc"?'desc':'asc').' class="sort">نویسنده'.($key == "uusername"?'<i class="fa fa-sort-'.($order == 'asc'?'up':'down').'"></i>':'').'</a></th>
				<th style="text-align: center">توضیحات</th>
			</tr>
	';
	foreach ($records as $recordInfo) //For scrolling 2D array 
	{
		$input = G2J($recordInfo['ppublish_date'],$lang);
		// count comment for per post
		$j = "";
		for($i=0;$i<=count($record3)-1;$i++)
		{
			if($record3[$i]['pid']==$recordInfo['pid'])
				$j++;
		}
		//cat of per post
		$title="";
		$c=0;
		for($p=0;$p<=count($records2)-1;$p++)
		{
			if($records2[$p]['pid']==$recordInfo['pid'])
			{
				$c++;
				$title .=$records2[$p]['ctitle'].' & ';
			}
		}
		//for delete & from end of cat 
		if($c>0)
			$title = substr_replace($title," ",-2);

		$content.= '
<tr id="'.$recordInfo['pid'].'">
   <td style="text-align: center"><input type="checkbox" name="pids[]" class="pids" value="'.$recordInfo['pid'].'"></td>
   <td style="text-align: center;white-space: nowrap;">
      '.$recordInfo['ptitle'].'<br>
      <hr>
      <a class="badge badge-info edit" href="?op=add&pid='.$recordInfo['pid'].'">ویرایش</a>
      &nbsp<a id="'.$recordInfo['pid']."qedit_button".'" class="badge badge-info" href="#">ویرایش سریع</a>
      &nbsp<a class="badge badge-danger confirm" href="?op=del&pid='.$recordInfo['pid'].'">حذف</a><br>
   </td>
   '.'
   <td dir="ltr" style="text-align: center">'.$recordInfo['puri'].'</td>
   <td dir="ltr" style="text-align: center">'.G2J($recordInfo['padd_date'],$lang).'</td>
   <td dir="ltr" style="text-align: center">'.G2J($recordInfo['pmod_date'],$lang).'</td>
   <td dir="ltr" style="text-align: right;">'.$title.'</td>
   <td dir="ltr" style="text-align: center;">'.$j.'</td>
   <td style="text-align: center">'.$recordInfo['uusername'].'</td>
   <td style="text-align: center">'.$recordInfo['pcomments'].'</td>
</tr>
<tr id="'.$recordInfo['pid']."qtr".'" class="slideup">
	<td colspan="4" style="padding-top:0;border:0 solid white;background-color:lightblue;text-align:right;">
		<div style="margin-right:35px;">
   		<h5><span class="badge badge-info">فرم ویرایش سریع</span></h5><br>
   		<form id="add_post_form" action = "" method="post" enctype="multipart/form-data">
	   		<ul style="list-style-type:none;">
	   			<li>
	   				<label for="ptitle">عنوان مطلب:</label>
	   			</li><br>
	   			<li>
	   				<input type="text" class="form-control col col-lg-7 ptitle" name="ptitle" id="'.$recordInfo['pid'].'ptitle" value="'.$recordInfo['ptitle'].'" autofocus>
	  		 	</li><br>
			   	<li style="margin-top:-10px;">
			   		<div id="ptitle_result'.$recordInfo['pid'].'"></div>
			   	</li>
	   		</ul><br>
		    <ul style="list-style-type:none; margin-top:-40px;">
	   			<li>
		   			<label for="puri">نامک مطلب:</label>
			    </li><br>
			    <li>
			    	<input type="text" class="form-control col col-lg-7 puri" name="puri" id="'.$recordInfo['pid'].'puri" dir="ltr" value="'.$recordInfo['puri'].'">
			    </li><br>
		   		<li style="margin-top:-10px;">
					<div id="puri_result'.$recordInfo['pid'].'"></div>
	   			</li><br>
	   			<li>
	   				<div id="puri_Rules" class="text-danger">موارد مجاز شامل حروف لاتین، اعداد، خط تیره می باشد.</div>
	  			</li>
	   		</ul><br>
	   		<ul style="list-style-type:none; margin-top:-20px;">
			   	<li>
		   			<label id="pvisible" for="pvisible">وضعیت مشاهده مطلب </label>
			   	</li>
			   	<li>
			   		<div class="form-inline" id="pvisible_items0'.$recordInfo['pid'].'">
			   		   '.$recordInfo['ppublic'].'
			   		</div>
		   		</li>
			   	<li> 
			   		<div class="form-group" id="pvisible_items1'.$recordInfo['pid'].'"></div>
			   	</li>
			   	<li> 
			   		<div class="form-group" id="pvisible_items2'.$recordInfo['pid'].'"></div>
			   	</li>
			   	<li>
			   		<div class="form-group col col-lg-8" id="pvisible_items3'.$recordInfo['pid'].'"></div>
			   	</li>
			   	<li>
			   		<div class="form-group col col-lg-8" id="pvisible_items4'.$recordInfo['pid'].'"></div>
			   		<span>در صورت خالی بودن از رمز قبلی استفاده خواهد شد.</span>
			   	</li>
	   		</ul>
	</td>
			<td colspan="5" style="padding-top:0;border:0 solid white;background-color:lightblue;text-align:right;">
				<ul  style="list-style-type:none; margin-top:65px;">
					<li>
				      	<label id="comment_status_label" for="comment_status_label">وضعیت دیدگاه مطلب</label>
					</li>
					<li style="margin-top:5px;">
						 <div id="comment_status_items'.$recordInfo['pid'].'">
						   '.$recordInfo['pcomment_status'].'
						</div>
					</li>
				</ul>
				<ul style="list-style-type: none;margin-top:10px;">
					<li>
						<label  id="post_publish_date_label" for="post_publish_date_label"> زمان انتشار مطلب</label>
					</li>
					<li style="margin-top:5px;">
						<div id="post_publish_date_items">
							<button id="ppublish_date'.$recordInfo['pid'].'" name="ppublish_date" type="button" class="btn btn-primary btn-sm ppublish_date">'.Englishtopersian(G2J($recordInfo['ppublish_date'],$lang)).'</button> <input  id="ppublish_date_now'.$recordInfo['pid'].'" name="ppublish_date_now"  type="hidden" class="btn btn-secondary btn-sm ppublish_date_now" value="پس از درج مطلب"> <div id="date_error'.$recordInfo['pid'].'"></div>
						</div>
					</li>
				</ul>
				<ul style="list-style-type: none;margin-top:-7px;">
					<li>
						<label  id="post_category_label" for="post_category_label">دسته ها</label>
					</li>
					<li style="margin-top:5px;">
						<label  id="cat_search_label" for="cat_search">جستجوی دسته‌ها:</label>
					</li>
					<li style="margin-top:5px;">
						<input type="text" id="cat_search" name="cat_search" class="form-control col col-lg-7">
					</li>
					<li style="margin-top:5px;">
						<span id="cats_result"></span>
					</li>
					<li>
						<span id="error_load_cat"></span>
					</li>
				</ul>
				<ul style="list-style-type: none" id="'.$recordInfo['pid'].'post_category_items">
				</ul>
	<tr id="'.$recordInfo['pid']."qtrr".'" class="slideup">
		<td colspan="9" style="padding-top:0;border:0 solid white;background-color:lightblue;">
			<button style="float:right" id="q_edit_cancel" name="q_edit_cancel" type="button" class="btn btn-secondary btn-sm">لغو</button>
			<button style="float:left;margin-left:40px;" name="q_edit_send" id="q_edit_send" type="button"  class="btn btn-success btn-sm">بروزرسانی</button>
			<div id="add_notice'.$recordInfo['pid'].'"></div>
		</td>
	</tr>
		</form>
    	</div>
			</td>
</tr>
		';
	}
	$content.= '
			<tr>
				<th style="text-align: center"> همه<br><input type="checkbox" class="select_all"></th>
				<th style="text-align: center">عنوان</th>
				<th style="text-align: center">نامک</th>
				<th style="text-align: center">تاریخ درج</th>
				<th style="text-align: center">تاریخ آخرین ویرایش</th>
				<th style="text-align: center">دسته(ها)</th>
				<th style="text-align: center">دیدگاه(ها)</th>
				<th style="text-align: center">نویسنده</th>
				<th style="text-align: center">توضیحات</th>
			</tr>
		</table>
		</form>
	</div>	
	';
	if($print==1)
		echo $content ;
}
function del($dir) //complete delete function
{
    if (!file_exists($dir)) 
        return true;
    if (!is_dir($dir)) //if only file exists
        return unlink($dir);// delete file
    foreach (scandir($dir) as $item)//scroll in directory
    {
        if ($item == '.' || $item == '..') 
            continue;
        if (!del($dir . DIRECTORY_SEPARATOR . $item)) 
            return false;
    }
    return rmdir($dir);// delete only empty folder
}
function G2J($dt,$lang = 'english',$i = '')//convert gregorian calendar to jalali calendar
{
	$dt = strtotime($dt);
	$dtFormat = "Y-m-d H:i:s";
	$dFormat = "F Y";
	if($i == "date_filter")// For specify date_filter ID
		$dt = jdate($dFormat,$dt);
	else
		$dt = jdate($dtFormat,$dt);
	date_default_timezone_set("UTC");
	if($lang == 'english')
		return $dt;
	else
		return tr_num($dt,'fa');
}
function J2G($dt,$lang='english',$i = '')
{
	$dt = explode(" ",$dt);
	$d = $dt[0];
	$t = $dt[1];
	$d = explode("-",$d);
	$t = explode(":",$t);
	$dt = jmktime($t[0],$t[1],$t[2],$d[1],$d[2],$d[0]);
	date_default_timezone_set("UTC");
	$dtFormat = "Y-m-d H:i:s";
	$dFormat = "Y-m";
	if($i == "date_filter")// For specify date_filter ID
		$dt = date($dFormat,$dt);
	else
		$dt = date($dtFormat,$dt);
	if ($lang=='english')
		return $dt;
	else
		return tr_num($dt,'fa');
}
?>