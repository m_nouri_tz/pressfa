<?php
session_start();
$isLoggedIn = 0;
//Session Hijacking
if(isset($_SESSION['user_last_ip']) === false)
{
	$_SESSION['user_last_ip'] = $_SERVER['REMOTE_ADDR'];
}
if($_SESSION['user_last_ip'] !== $_SERVER['REMOTE_ADDR'])
{
	session_unset();
	session_destroy();
}
//Inlcude Config.php From Root (Database Infromation)  
require_once($_SERVER['DOCUMENT_ROOT'] . 'config.php');
//Off Display Error
if($dev == 0)
	error_reporting(0);
//Inlcude class.database.php From Root Database Function Connect,Add,Delete,Update,List 
require_once($_SERVER['DOCUMENT_ROOT'] . 'includes/class.database.php');
//Check If Not Install Pressfa (If Not Connect To Database)
if(!$link)
{
	header('location:../install');
	exit();
}
//Inlcude functions.php From Root (Public Functions)
require_once($_SERVER['DOCUMENT_ROOT'] . 'includes/functions.php');
//Inlcude jdf.php From Root (Jdf Library)
require_once($_SERVER['DOCUMENT_ROOT'] . 'includes/jdf.php');
//Inlcude english.php From Root (Language File)
require_once($_SERVER['DOCUMENT_ROOT'] . 'language/english.php');
//Check Cookie OR Session(Check Content Cookie OR Session)
if(isset($_COOKIE['mypressfa']) || isset($_SESSION['mypressfa']))
{
	if(isset($_COOKIE['mypressfa']))
		$auth = $_COOKIE['mypressfa'];
	else
		$auth = $_SESSION['mypressfa'];
	$auth = explode(":",$auth);
	$userInfo = DB::ListRecords("*","users",$where = "WHERE uusername=:uusername AND upass=:upass",$whereArray=array("uusername"=>$auth[0],"upass"=>$auth[1]));
	if(count($userInfo) == 1)
	{
		$isLoggedIn = 1;
	}
	else
	{
		header('location:../logout.php');
	}
}
// else
// {
// 	//Check if befor User Directed To Login.php-->Check Address bar
// 	if(strpos($_SERVER['REQUEST_URI'], "/login.php")===false)
// 	{
// 	header('location: ../login.php');	
// 	}
// }
// CHECK IF We are in login.php
// elseif(isset($_POST['login']))
// {
// 	echo 'login posted!';
// 	if(strpos($_SERVER['REQUEST_URI'], "/login.php")===false)
// 	{
// 	header('location: ../login.php');
// 	}
// }
?>