<?php
//Panel Admin 
echo '<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>pressfa</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<link rel="stylesheet" type="text/css" href="themes/default/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="themes/default/css/bootstrap-rtl.min.css">
	<link rel="stylesheet" type="text/css" href="themes/default/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="themes/default/css/style.css">
  <link rel="stylesheet" type="text/css" href="themes/default/css/jquery.md.bootstrap.datetimepicker.style.css">
	<script src="themes/default/js/jquery.min.js"></script>
  <script src="themes/default/js/tinymce/tinymce.min.js" referrerpolicy="origin"></script>
  <script src="themes/default/js/popper.min.js"></script>
	<script src="themes/default/js/bootstrap.min.js"></script>
	<script src="themes/default/js/all.min.js"></script>
	<script src="themes/default/js/script.js"></script>
  <script src="themes/default/js/jquery.md.bootstrap.datetimepicker.js"></script>
  <script src="themes/default/js/tinymce/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>
  <script>
  editor("rtl");
  </script>
</head>
<body dir="rtl" class="bg-info">
';
//Check Admin Login
if($isLoggedIn === 1)
{
	echo '<nav class="navbar navbar-expand-lg  navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="#">پرسفا</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-sticky-note"></i> مطالب
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="post.php?op=add">افزودن مطلب جدید</a>
          <a class="dropdown-item" href="post.php?op=list">همه مطالب</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          صفحات
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن صفحه جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش صفحات</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          دسته ها
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن دسته جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش دسته&zwnj;ها</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           فهرست&zwnj;ها
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن فهرست جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش فهرست&zwnj;ها</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           گالری
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن گالری جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش گالری&zwnj;ها</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           پیوند&zwnj;ها
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن پیوند جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش پیوند&zwnj;ها</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           اسلایدر&zwnj;ها
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن اسلایدر جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش اسلایدر&zwnj;ها</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           فرم&zwnj;ها
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن فرم جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش فرم&zwnj;ها</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           فایل&zwnj;ها
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">افزودن فایل جدید</a>
          <a class="dropdown-item" href="#">لیست و ویرایش فایل&zwnj;ها</a>
        </div>
      </li>
    </ul>
   <!--  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
    <div class="btn-group">
      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-cog"></i>
      </button>
      <div class="dropdown-menu  dropdown-menu-right">
        <a class="dropdown-item" href="/logout.php">خروج</a>
        <a class="dropdown-item" href="/">بازدید سایت</a>
      </div>
    </div>
  </div>
</nav>';
}
// else
// {
//   //User Login Show User panel
//   echo '
//   <nav class="navbar navbar-expand-lg  navbar-dark bg-dark fixed-top">
//   <a class="navbar-brand" href="#">پرسفا</a>
//   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//     <span class="navbar-toggler-icon"></span>
//   </button>
//   <div class="collapse navbar-collapse" id="navbarSupportedContent">
//     <ul class="navbar-nav mr-auto">
//       <li class="nav-item dropdown">
//         <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//           <i class="fas fa-sticky-note"></i>ویرایش پروفایل
//         </a>
//       </li>
//     <div class="btn-group">
//       <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//         <i class="fas fa-cog"></i>
//       </button>
//       <div class="dropdown-menu  dropdown-menu-right">
//         <a class="dropdown-item" href="/logout.php">خروج</a>
//         <a class="dropdown-item" href="/">بازدید سایت</a>
//       </div>
//     </div>
//   </div>
// </nav>
//   ';
// }
echo '
	<main role="main" class="container">
	  <div class="jumbotron">

';