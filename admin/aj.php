<?php
$_SESSION['addpostcat'][]=1;//if user not select any cat
require_once 'main.php';//check security
//default==>2 or more space =>1 space 
//or
//everey character replace by space
function Trimer($string,$charOld='/ * /',$charNew = " ")
{
	return preg_replace($charOld, $charNew, $string);
}
//convert calendar jalali(shamsi) to gregorian(miladi)
// function J2G($dt,$lang = 'english')
// {
// 	$dt = explode(" ", $dt);
// 	$d = $dt[0];
// 	$t = $dt[1];
// 	$d = explode("-", $d);
// 	$t = explode(":", $t);
// 	$dt = jmktime($t[0],$t[1],$t[2],$d[1],$d[2],$d[0]);
// 	date_default_timezone_set("UTC");
// 	$dt = date("Y-m-d H:i:s",$dt);
// 	if($lang == 'english')
// 		return $dt;
// 	else
// 		return tr_num($dt,'fa');
// }
//convert persian number to english number
function Persiantoenglish(&$input) 
{
  $inputstring = [];
  $inputstring = $input;
  $persian = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
  $english = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
  for($j = 0;$j<count($inputstring);$j++)
  {
    for ($i = 0; $i < 10; $i++) 
    {
        $inputstring = str_replace($persian[$i], $english[$i],$inputstring);
    }
  }
  return $inputstring;
}
//Valider regex
function Valider($reg,&$string)
{
	if (preg_match("$reg", $string)) 
	    return 1;
	else 
	    return 0;
}
switch ($_POST['op'])
{
	case 'selectcat':
		$fieldId = $_POST['fieldId'];
		$fieldName = trim(Trimer($_POST['fieldName']));
		$tableName = trim(Trimer($_POST['tableName']));
		$where = "where $fieldName=:$fieldName";
		$whereArray = array($fieldName=>$fieldId[0]);
		$catRecord = DB::ListRecords('*',$tableName,$where,$whereArray);
		$catsRecord = [];
		foreach ($catRecord as $recordsCount2) 
		{
			array_push($catsRecord,$recordsCount2['cid']);
			echo $recordsCount2['cid']."selectcat";
		}
	break;
	case 'deleteposts':
		$fieldValue = trim(Trimer($_POST['fieldValue']));
		if(!isset($_POST['op2']))
		{
			if(!in_array($fieldValue, $_SESSION['deleteposts']))// add post to cat
			{
				$_SESSION['deleteposts'][] = $fieldValue;
			}
		}
		else
		{
			if (($key = array_search($fieldValue, $_SESSION['deleteposts'])) !== false)//delete post in cat
			{
			    unset($_SESSION['deleteposts'][$key]);
			}
		}
	break;
	//Check availability exp:puri,cat title
	case 'Checkavailability':
		sleep(1);//sleep 1 second
		//check if value empty
		if($_POST['fieldValue'] == "")
		{
			echo $_POST['fieldName'].'Empty';//answer ajax with echo
			$_SESSION['URI'] = $_POST['fieldName'].'Empty';
		}
		//check if value valid or no
		else if(Valider("/^[A-Za-z0-9-_ ]*$/",$_POST['fieldValue'])==0 && ($_POST['fieldName']=='puri'))
		{
			echo $_POST['fieldName'].'Invalid';
			$_SESSION['URI']=$_POST['fieldValue'].'Invalid';
		}
		else
		{
			//get parameter from ajax
			$tableName = $_POST['tableName'];
			$fieldName = $_POST['fieldName'];
			$fieldValue = $_POST['fieldValue'];
			$where = "where $fieldName=:$fieldName";
			$whereArray = array($fieldName=>$fieldValue);
			$recordsCount = count(DB::ListRecords('*',$tableName,$where,$whereArray));
			if($recordsCount==0)//mean value is valid(not duplicate)
			{
				$_SESSION['URI'] = $_POST['fieldName'].'Valid';
				if(isset($_POST['op2']))
					echo $_POST['fieldName'].'Validclick';//submit new cat to database
				else
				echo $_POST['fieldName'].'Valid';
			}
			else
			{
				$_SESSION['URI'] = $_POST['fieldName'].'Duplicate';//value is duplicate
				echo $_POST['fieldName'].'Duplicate';
			}
		}
	break;
	//check publish date
	case 'Checkdate':
		$_POST['fieldValue'] = Persiantoenglish($_POST['fieldValue']);//because php not work persian number=>convert to english number
		$fieldValue = trim(Trimer($_POST['fieldValue']));//2or more space =>one space
		if($_POST['fieldValue'] == "")
		{
			echo $_POST['fieldName'].'Empty';
			$_SESSION['PUBLISHDATE'] = $_POST['fieldName'].'Empty';
		}
		else if(Valider("/^[0-9- :]+$/",$_POST['fieldValue']) == 0)
		{
			echo $_POST['fieldName'].'Invalid';
			$_SESSION['PUBLISHDATE'] = $_POST['fieldName'].'Invalid';
		}
		else if(date("Y-m-d H:i:s")>J2G($_POST['fieldValue']))//check if publish date less than now date
		{
			echo $_POST['fieldName'].'lt';
			$_SESSION['PUBLISHDATE'] = $_POST['fieldName'].'lt'.'  '.J2G($_POST['fieldValue']);// if checkbox of publish date confirm checked
		}
		else
		{
			echo $_POST['fieldName'].'gt';//publish date greather than now date
			$_SESSION['PUBLISHDATE'] = $_POST['fieldName'].'gt'.'  '.J2G($_POST['fieldValue']);
		}	
	break;
	case 'Search'://search and list exp:cat,tags
		$tableName = $_POST['tableName'];
		$fieldName = $_POST['fieldName'];
		$fieldValue = trim(Trimer($_POST['fieldValue']));//2or more space =>one space
		if(isset($_POST['op3']))
		{
			if(!isset($_SESSION['tags']))//first time session tag
			{
				$list = DB::ListRecords('*',$tableName);
				foreach ($list as $list2) 
				{
						$_SESSION['tags'][] = $list2['ttitle'];
				}
			}
			else
			{
				$pattern = "/".$fieldValue."/i";
				for($i = 0;$i<=(count($_SESSION['tags']))-1;$i++)
				{
					if(preg_match($pattern, $_SESSION['tags'][$i]))
					{
							if(!in_array($_SESSION['tags'][$i], $_SESSION['posttags']))//Other than posttags
								echo $_SESSION['tags'][$i]."tag  ";
					}
				}
			}
		}
		//if user click on X
		elseif($tableName == 'tags' && !isset($_POST['op3']))
		{
			if(isset($_POST['op2']))
			{
				if (($key = array_search($fieldValue, $_SESSION['posttags'])) !== false)//delete from posttags
				{
				    unset($_SESSION['posttags'][$key]);
				} 
			}
			else
			{// add to session posttags
				if(!isset($_SESSION['posttags']))
				{
					$_SESSION['posttags']="";
					$_SESSION['posttags'][] = $fieldValue;
					echo 'tagnotDuplicate'."  ".$fieldValue;
					
				}
				else
				{
					 if(in_array($fieldValue, $_SESSION['posttags']))
					 {
					 	echo 'tagDuplicate';
					 }
					 else
					 {
					 	$_SESSION['posttags'][] = $fieldValue;
					 	echo 'tagnotDuplicate'."  ".$fieldValue;
					 }
				}
			}
			
		}
		else
		{
			$where = "WHERE $fieldName LIKE :$fieldName";
			$whereArray = array($fieldName=>"%$fieldValue%");
			$recordsCount = DB::ListRecords('*',$tableName,$where,$whereArray);
			if(!empty($recordsCount))
			{
				foreach ($recordsCount as $recordsCount2) 
				{
					// if($tableName === 'tags')
					// {
					// 	$recordsCount2['ttitle'].="tag  ";
					// 	print_r($recordsCount2['ttitle']);
					// }
					if($tableName === 'categories')//list cats
					{
						$pack_search = [];
								array_push($pack_search,$recordsCount2['cid'].="  scat  ");
								array_push($pack_search,$recordsCount2['ctitle'].="  scat  ");
								array_push($pack_search,$recordsCount2['curi'].="  scat  ");
								array_push($pack_search,$recordsCount2['cparent'].="  scat  ");
								array_push($pack_search,"cat-   ");
						print_r($pack_search);
					}
				}
			}
			else
				echo 'Nothing';//error list
		}
		
	break;
	case 'Samplelist'://list of parent cats & newtags
		$tableName = $_POST['tableName'];
		$fieldValue = trim(Trimer($_POST['fieldValue']));
		if($tableName === 'tags')
		{

			if(!in_array($fieldValue, $_SESSION['tags']))
			{
				$_SESSION['newtags'][] = $fieldValue;
			}
		}
		elseif($tableName === 'categories')
		{
			$list = DB::ListRecords('*',$tableName);
			$pack = [];
			if(!empty($list))
			{
				foreach ($list as $list2) //list  cats comobox
				{
					array_push($pack,$list2['cid'].="cat   ");
					array_push($pack,$list2['ctitle'].="cat   ");
					array_push($pack,$list2['curi'].="cat   ");
					array_push($pack,$list2['cparent'].="cat   ");
					array_push($pack,"cat-   ");
				}
				print_r($pack);
			}
			else
				echo 'CatListError';
		}

	break;
	case 'addpostcat'://add post to cat
		$fieldValue = trim(Trimer($_POST['fieldValue']));
		if($fieldValue=='set 1')//default value for addpostcat =>onchange
		{
			//unset session if user refresh page
			unset($_SESSION['URI']);
			unset($_SESSION['PUBLISHDATE']);
			unset($_SESSION['newtags']);
			unset($_SESSION['posttags']);
			unset($_SESSION['tags']);
			unset($_SESSION['addpostcat']);
			unset($_SESSION['PAGING']);
			unset($_SESSION['deleteposts']);
			if(!in_array(1, $_SESSION['addpostcat']))
			{
				$_SESSION['addpostcat'][] = 1;
			}
		}
		else
		{
			if(Valider("/^[0-9]+$/",$fieldValue)==0)//check name of post should be number
				echo 'AddPostCatError';
			else
			{

				if(!isset($_POST['op2']))
				{
					if(!in_array($fieldValue, $_SESSION['addpostcat']))// add post to cat
					{
						$_SESSION['addpostcat'][] = $fieldValue;
					}
				}
				else
				{
					if (($key = array_search($fieldValue, $_SESSION['addpostcat'])) !== false)//delete post in cat
					{
					    unset($_SESSION['addpostcat'][$key]);
					}
				}
			}
		}
		
	break;
	case 'addnewcat'://add new cat
		$fieldValue = trim(Trimer($_POST['fieldValue']));
		$pack2[] = $fieldValue;
		$cparent = trim(Trimer($_POST['cparent']));
		foreach ($pack2 as $value) 
		{
			$params=array(
				'uid'=>$userInfo[0]['uid'],
				'ctitle'=>$value,
				'curi'=>$value,
				'cparent'=>$cparent,
				'cadd_date'=>date("Y-m-d H:i:s"),
				'cmod_date'=>date("Y-m-d H:i:s"),
			);
			$last_id_category_post = DB::AddRecord($params,'categories');
			if(empty($last_id_category_post))
				echo 'AddNewCatError';
			else
				echo 'AddNewCatSuccess';
		}
	break;	
	//Check Image Upload 
	case 'Checkimageupload':
		// $tmp_path = $_FILES['pimage']['tmp_name'];
		// $pimage = basename($_FILES['pimage']['name']);//Get Orginial name of Image 
		// echo $pimage;
		// //Get Image Extention
		// $ext = explode('.',$pimage);
		// $ext = $ext[count($ext)-1];
		// $pimage = md5(uniqid().$pimage ).'.'.$ext; 
		// $whitelist = array('jpg','png');// list of Extention Valid image
		sleep(1);//sleep 1 second
		//Check Image Upload if value empty
		if($_POST['fieldValue'] == '')
		{
			echo $_POST['fieldName'].'Empty';//answer ajax with echo
			$_SESSION['IMAGEUPLOAD'] = $_POST['fieldName'].'Empty';
		}
		elseif($_POST['fieldValue'] != '') 
		{
			$ext = explode('.',$_POST['fieldValue']);// C:\fakepath\b.png
			$ext = $ext[count($ext)-1];// png
			//check if value valid or no   
		 	if(( $ext === 'png' || $ext === 'jpg')) // && ($_POST['fieldName']=='pimage')
			{
				echo $_POST['fieldName'].'Valid';//$ext
				$_SESSION['IMAGEUPLOAD']=$_POST['fieldValue'].'Valid';
			}
			elseif(($ext !== 'png' || $ext !== 'jpg')) // && ($_POST['fieldName']=='pimage')
			{
				// echo $ext;// $_POST['fieldName'].'Invalid'
				echo $_POST['fieldName'].'Invalid';//$ext
				$_SESSION['IMAGEUPLOAD']=$_POST['fieldValue'].'Invalid';
			}
		}
		// else
		// {
		// 	//get parameter from ajax
		// 	$tableName = $_POST['tableName'];
		// 	$fieldName = $_POST['fieldName'];
		// 	$fieldValue = $_POST['fieldValue'];
		// 	$where = "where $fieldName=:$fieldName";
		// 	$whereArray = array($fieldName=>$fieldValue);
		// 	$recordsCount = count(DB::ListRecords('*',$tableName,$where,$whereArray));
		// 	if($recordsCount==0)//mean value is valid(not duplicate)
		// 	{
		// 		$_SESSION['IMAGEUPLOAD'] = $_POST['fieldName'].'Valid';
		// 		if(isset($_POST['op2']))
		// 			echo $_POST['fieldName'].'Validclick';//submit new cat to database
		// 		else
		// 		echo $_POST['fieldName'].'Valid';
		// 	}
		// 	else
		// 	{
		// 		$_SESSION['IMAGEUPLOAD'] = $_POST['fieldName'].'Duplicate';//value is duplicate
		// 		echo $_POST['fieldName'].'Duplicate';
		// 	}
		// }
	break;	
}
?>