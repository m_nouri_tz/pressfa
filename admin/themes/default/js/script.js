  var postId,ptitleId,ptitleIdNumber,puriId,puriIdNumber,ppublishId,ppublishIdNumber;
  var checkboxs = [];
  //default==>2 or more space =>1 space 
  //or
  //everey character replace by space
  function Trimer(string,charOld=/ * /g,charNew = " ")
  {
   return string = (string.trim()).replace(charOld,charNew);
  }
  //Valider regex
  function Valider(reg,string)
  {
    if(reg.test(string))
      return 1;
    else
      return 0;
  }
  //convert 2 or more - => space
  //then
  //convert 2 or more space=>one space
  //then
  //conver 1 space =>one -
  function Urireplacer(myelement)
  {
    if($(myelement).val().indexOf("-")!==-1)
      $(myelement).val(Trimer($(myelement).val(),/ *-/g));
    $(myelement).val(Trimer($(myelement).val()));
    $(myelement).val((Trimer($(myelement).val(),/ * /g,"-")).toLowerCase());
  }
  //randome number 7 length function
  function RandomeNumber()
  {
    var d = new Date();
    var n = (d.getTime()).toString();
    n = n.substring(n.length - 7, n.length);
    return n;
  }
  //Addnoticeerror function
  function Addnoticeerror(noticeButton)
  {
    var AddNoticeErrorMsg = 'خطایی در افزودن مطلب رخ داد!';
    $("#"+noticeButton).html(Error(AddNoticeErrorMsg));
  }
  //Add tiny mce
  function editor(dir){
    tinymce.init({
      selector: '.editor',
      plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable tiny_mce_wiris code responsivefilemanager',
      tinydrive_token_provider: 'URL_TO_YOUR_TOKEN_PROVIDER',
      tinydrive_dropbox_app_key: 'YOUR_DROPBOX_APP_KEY',
      tinydrive_google_drive_key: 'YOUR_GOOGLE_DRIVE_KEY',
      tinydrive_google_drive_client_id: 'YOUR_GOOGLE_DRIVE_CLIENT_ID',
      mobile: {
        plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker textpattern noneditable help formatpainter pageembed charmap mentions quickbars linkchecker emoticons advtable tiny_mce_wiris code responsivefilemanager'
      },
      menu: {
        tc: {
          title: 'TinyComments',
          items: 'addcomment showcomments deleteallconversations'
        }
      },
      menubar: 'file edit view insert format tools table tc help',
      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment |  tiny_mce_wiris_formulaEditor | tiny_mce_wiris_formulaEditorChemistry | code | responsivefilemanager',
      autosave_ask_before_unload: true,
      autosave_interval: '30s',
      autosave_prefix: '{path}{query}-{id}-',
      autosave_restore_when_empty: false,
      autosave_retention: '2m',
      image_advtab: true,
      link_list: [
      { title: 'My page 1', value: 'http://www.tinymce.com' },
      { title: 'My page 2', value: 'http://www.moxiecode.com' }
      ],
      image_list: [
      { title: 'My page 1', value: 'http://www.tinymce.com' },
      { title: 'My page 2', value: 'http://www.moxiecode.com' }
      ],
      image_class_list: [
      { title: 'None', value: '' },
      { title: 'Some class', value: 'class-name' }
      ],
      importcss_append: true,
      templates: [
      { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
      { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
      { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
      ],
      template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
      template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
      height: 600,
      image_caption: true,
      quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
      noneditable_noneditable_class: 'mceNonEditable',
      toolbar_mode: 'sliding',
      spellchecker_whitelist: ['Ephox', 'Moxiecode'],
      tinycomments_mode: 'embedded',
      content_style: '.mymention{ color: gray; }',
      contextmenu: 'link image imagetools table configurepermanentpen',
      a11y_advanced_options: true,
      directionality:dir,
      language:'fa',
      external_filemanager_path:"themes/default/js/tinymce/plugins/responsivefilemanager/filemanager/",
      filemanager_title:"Responsive Filemanager" ,
      external_plugins: { "filemanager" : "themes/default/js/tinymce/filemanager/plugin.min.js"}
    });
  }
  //Error message client side
  function Error(message)
  {
    return "<small class = \"badge badge-danger\"><i class = \"fas fa-times\"></i> " + message + "</small>";
  }
  //Success message client side
  function Success(message)
  {
    return "<small class = \"badge badge-success\"><i class = \"fas fa-check\"></i> " + message + "</small>";
  }
  //Ajax Function
  function Ajax(d,resDIV,successMessage = '',errorMessage = '')
  {
    // $("#"+resDIV).html('<div id="spinner" class="spinner-grow text-primary spinner-grow-sm" role="status"> <span class="sr-only">Loading...</span> </div>');//Spinner Add
    $.ajax({
      url: "aj.php",//path ajax go parameter
      type: "POST",
      async: false,// two ajax dont go together
      data: d,//send data
      success: function(data,status)//result of ajax
      {
        result = data;
      },
      error: function(){alert("مشکلی در اتصال به سرور به وجود آمد!")}//Error Ajax
    });
    // if(result.indexOf("selectcat")==-1)
    // {
    // $("#spinner").remove();//remove spinner
    // }
    if(result.indexOf("selectcat")!=-1)
    {
      result=result.split("selectcat");
      $("#"+resDIV+" #1").prop('checked', false);
      for(var i = 0;i<result.length-1;i++)
      {
        $("#"+resDIV+" #"+result[i]).prop('checked', true);
      }
    }
    if(result.indexOf("AddPostCatError")!==-1)//if in add post to cat Error
      $("#"+resDIV).html(Error("دسته انتخاب شده غیرمجاز است!"));
    if(result.indexOf("tagnotDuplicate")!==-1)//if tag not duplicate
    {
      var res = result.split("  ");
      //add randome id to tag
      n = res[1]+"_"+RandomeNumber();//get main name tag
      $("#usertags").append('<div id="'+n+'" style = "display:inline-block;" class="alert alert-success alert-dismissible fade show" role="alert"><strong>'+res[1]+'</strong><button id="closebutton_'+n+'" type="button" class="close" data-dismiss="alert" aria-label="Close"><span id="closespan_'+n+'" aria-hidden="true">&times;</span></button></div>');//add tag to post
    }
    if(result.indexOf("tagDuplicate")!==-1)//if tag  duplicate
      $("#ttitle_search").val("");//search tag empty
    if(result=='ppublish_dateEmpty' || result=='ppublish_dateInvalid')//if publish date empty or publish date invalid
    {
      event.preventDefault();//cancel event(click)(function)
      Addnoticeerror("add_notice"+ppublishIdNumber);
      $('#ppublish_date_now'+ppublishIdNumber).attr('type','visible');//visibilty button publish now after add/update post
      $("#"+resDIV).html(Error("تاریخ وارد شده نامعتبر است!"));
    }
    if(result == 'ppublish_datelt')//if user publish date less than now date system
    {
      event.preventDefault();//cancel event(click)
      Addnoticeerror("add_notice"+ppublishIdNumber);//under button add or update
      $("#"+resDIV).html(Error("تاریخ انتشارمطلب انتخاب شده از تاریخ فعلی سیستم کوچکتر است.")+'<div class="form-group"><input form="add_post_form" type="checkbox" id="date_accept'+ppublishIdNumber+'" name="date_accept"> <label id="date_accept_label" for="date_accept"></label>تاریخ انتشار مطلب وارد شده مورد تایید است.</div>');//show checkbox & message
      $('#ppublish_date_now'+ppublishIdNumber).attr('type','button')//visibilty button publish now after add/update post
    }
    if(result == 'ppublish_dategt')//if user publish date grether than now date system
    {
      $("#date_error"+ppublishIdNumber).html('');
      $('#ppublish_date_now'+ppublishIdNumber).attr('type','button');
    }
    if(result=='puriEmpty')//if post uri empty
    {
      event.preventDefault();
      Addnoticeerror("add_notice"+puriIdNumber);
      $("#"+resDIV).html(Error('نامک مطلب نمی تواند خالی باشد!'));
    }

    if(result=='puriEmpty')//if post uri empty
    {
      event.preventDefault();
      Addnoticeerror("add_notice"+puriIdNumber);
      $("#"+resDIV).html(Error('نامک مطلب نمی تواند خالی باشد!'));
    }

    if(result=='ctitleEmpty')//in add new cat if empty cat title
    {
      event.preventDefault();
      $("#"+resDIV).html(Error('نام دسته جدید نمی تواند خالی باشد!'));
    }
    if(result=='puriInvalid')//if post uri invalid
    {
      event.preventDefault();
      Addnoticeerror("add_notice"+puriIdNumber);
      $("#"+resDIV).html(Error('نامک وارد شده نامعتبر است!'));
    }

    if(result=='pimageInvalid')//if post uri valid
    {
      event.preventDefault();
      Addnoticeerror();
      $("#"+resDIV).html(Error('پسوند تصویرشاخص مجازنیست. پسوندهای مجاز: jpg,png'));
    }

    if(result=='puriValid')//if post uri valid
    {
      $("#"+resDIV).html(Success(''));
    }

    if(result=='pimageValid')//if post uri invalid   'png'
    {
      // alert(result);
      $("#"+resDIV).html(Success(''));
    }

    if(result=='AddNewCatError')//add new cat error
      $("#"+resDIV).html(Error('افزودن دسته جدید با خطا مواجه شد!'));
    if(result=='AddNewCatSuccess')//add new cat success
    {
      //refresh cats and parent cats
      var data = {op:"Samplelist",tableName:"categories"};
      Ajax(data,"post_category_items_select","","خطایی در دسترسی به دسته ها رخ داد( کد خطا: L1)!");
      var data = {op:"Samplelist",tableName:"categories"};
      Ajax(data,"post_category_items","","خطایی در دسترسی به دسته ها رخ داد( کد خطا: L1)!");

    }
    if(result=='puriDuplicate')//if post uri Duplicate
    {
      event.preventDefault();
      Addnoticeerror("add_notice"+puriIdNumber);
      $("#"+resDIV).html(Error('نامک وارد شده تکراری است!'));
    }
    if(result=='ctitleValidclick')//if click button add new cats(Clicked!)
    {
      var data = {op:"addnewcat",tableName:"categories",fieldValue:$("#ctitle").val().trim(),cparent:$("#post_category_items_select").find('option:selected').val()};
      Ajax(data,"add_new_category_result");
    }
    if(result=='ctitleDuplicate')//if new cat name is duplicate
    {
      event.preventDefault();
      $("#"+resDIV).html(Error('نام دسته جدید نمی تواند تکراری باشد!'));
    }
    else if(result.indexOf('tag  ') != -1)//list of tags(blue)
    {
      var res = result.split("tag  ");
      for(var i = 0;i<res.length-1;i++)
      {
        n = res[i].replace(/\s/g, '')+"_"+RandomeNumber();
        $("#tag_result").append('<a  href="#" id="'+n+'" class="list-group-item list-group-item-action list-group-item-info">'+res[i]+'</a>');
      }
    }
    else if(result.indexOf("  scat  ")!==-1)//list of cats
    {
      var pack = [];
      pack = result.split("cat-   ");
      for(var i = 0;i<pack.length-1;i++)
      {
        var res = pack[i].split("  scat  ");
        $("#"+resDIV).append('<li>'+'|'+'<input type="checkbox"'+(((((res[1].split(">")))[1]).trim())=='دسته اصلی (پیشفرض)'?'checked="checked"':'')+ 'id = "'+((((res[0].split(">")))[1]).trim())+'" name="'+((((res[0].split(">")))[1]).trim())+'"> <label style="display:inline;" for="'+((((res[2].split(">")))[1]).trim())+'">'+((((res[1].split(">")))[1]).trim())+'</label></li>');  
      }
    }
    else if(result == 'CatListError')//list of cats error
    {
      $("#error_load_cat").html(Error(errorMessage));
      $("#add_category_button").remove();
      $("#post_category_items").remove();
    }
    if(result.indexOf("cat  ")!==-1)//list of parent cats
    {
      var hasChilds;
      var space = 0;
      var parent = 0;
      var str = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
      var pack = [];
      var res = [];
      pack = result.split("cat-   ");
      Listcats(parent,pack);
      function Listcats(parent,pack)
      {
        hasChilds = false;
        for(var i = 0;i<(pack.length)-1;i++)
        {
          res = pack[i].split("cat  ");
          if (((((res[3].split(">")))[1]).trim())== parent)
          {
            if (hasChilds === false)
            {
              if(((((res[1].split(">")))[1]).trim())!=="دسته اصلی (پیشفرض)")
                space++;
              hasChilds = true;
            }
            if(resDIV == 'post_category_items_select')
            {
              $("#"+resDIV).append('<option'+' value="'+((((res[0].split(">")))[1]).trim())+'">'+str.repeat(space)+'|'+((((res[1].split(">")))[1]).trim()));
              Listcats(((((res[0].split(">")))[1]).trim()),pack);
              $("#"+resDIV).append('</option>');
            }
            else if(resDIV.indexOf("post_category_items")!==-1)
            {
              $("#"+resDIV).append('<li>'+str.repeat(space)+'|'+'<input   class="cats"  type="checkbox"'+(((((res[1].split(">")))[1]).trim())=='دسته اصلی (پیشفرض)'?'checked="checked"':'')+ 'id = "'+((((res[0].split(">")))[1]).trim())+'" name="'+((((res[0].split(">")))[1]).trim())+'"> <label style="display:inline" for="'+((((res[2].split(">")))[1]).trim())+'">'+((((res[1].split(">")))[1]).trim())+'</label></li>');  
              Listcats(((((res[0].split(">")))[1]).trim()),pack);
            }
          }
        }
        if (hasChilds === false) if(space != 0)space-- ;
      }
    }
  }
  $(document).ready(function()
  {
    $(".pids:checkbox:not(:checked)").each(function () 
    {
      checkboxs.push($(this).val());
    });
    //check any post selected or no
    $('#multipledelete').click(function(){
      if($(".pids:checkbox:checked").length==0)
      {
        event.preventDefault();;
        $("#delete_error").html(Error("لطفا پست( هایی) را برای حذف کردن انتخاب کنید!"));
      }
    })
    $('.select_all').change(function()//if selectall checkbox clicked! 
    {
      if($(this).is(":checked")) 
      {
        $('.pids,.select_all').prop('checked', true);//check all checkbox with pids class
        $(".pids:checkbox:checked").each(function() {// insert checkbox val to session one by one
          var data = {op:"deleteposts",fieldValue:$(this).attr("value")};
          Ajax(data);
        });
      }
      else
      {
        $('.pids,.select_all').prop('checked', false);//uncheck all checkbox with pids class
        $(".pids:checkbox:not(checked)").each(function() {// delete checkbox val from session one by one
          var data = {op:"deleteposts",fieldValue:$(this).attr("value"),op2:"deleteposts"};
          Ajax(data);
        });
      }
      });
    $('.pids').change(function()//if checkbox clicked! 
    {
      if($(this).is(":checked")) 
      {
        var data = {op:"deleteposts",fieldValue:$(this).val()};// insert to session checkbox val
        Ajax(data);
      }
      else
      {
        var data = {op:"deleteposts",fieldValue:$(this).val(),op2:"deleteposts"};// delete from session checkbox val
        Ajax(data);
      }
      });
    $("#list_table").on("click",function()
    {
      if((event.target.id).indexOf("qedit_button") !==-1 )//if click on qedit_button(quick edit)
      {
        postId = event.target.id.split("qedit_button")[0];
        if($("#"+(event.target.id).split("qedit_button")[0]+"post_category_items").html().trim()=="")
        {
          var data = {op:"Samplelist",tableName:"categories"};
          Ajax(data,(event.target.id).split("qedit_button")[0]+"post_category_items","","خطایی در دسترسی به دسته ها رخ داد( کد خطا: L1)!");
          var data = {op:"selectcat",fieldId:event.target.id.match(/\d+/),tableName:"category_post",fieldName:"pid"};// insert to session checkbox val
          Ajax(data,(event.target.id).split("qedit_button")[0]+"post_category_items");
        }
        $("#"+(event.target.id).split("qedit_button")[0]+"qtr").fadeToggle();//open tr with quick edit form
        $("#"+(event.target.id).split("qedit_button")[0]+"qtrr").fadeToggle();//open tr with update and cancel button
      }
      //set post visibility with radio button
       if($("#"+"pvisible_items0"+(event.target.id).split("qedit_button")[0]).html()==0)
       {
        $("#"+"pvisible_items0"+(event.target.id).split("qedit_button")[0]).html('<input checked type="radio" id="ppublic" name="pvisible" value="0">&nbsp<label style="display:block" for="ppublic">عمومی(همه)</label>');
        $("#"+"pvisible_items1"+(event.target.id).split("qedit_button")[0]).html('<input type="radio" id="pprivate" name="pvisible" value="1"> <label style="display:inline;" for="pprivate">خصوصی</label>')
        $("#"+"pvisible_items2"+(event.target.id).split("qedit_button")[0]).html('<input type="radio" id="ppassword" name="pvisible" value="2"> <label  style="display:inline;" for="ppassword">رمزدار</label>');
        $("#"+"pvisible_items3"+(event.target.id).split("qedit_button")[0]).html('<div id="ppassword_div" > <label style="display:block;padding-top:5px;" id="ppassword_label"  for="ppass">رمز عبور:</label> ');
        $("#"+"pvisible_items4"+(event.target.id).split("qedit_button")[0]).html('<input  type="text" class="form-control" name="ppass" id="'+event.target.id.split("qedit_button")[0]+'ppass">');
       }
       else if($("#"+"pvisible_items0"+(event.target.id).split("qedit_button")[0]).html()==1)
       {
         $("#"+"pvisible_items0"+(event.target.id).split("qedit_button")[0]).html('<input  type="radio" id="ppublic" name="pvisible" value="0">&nbsp<label style="display:block" for="ppublic">عمومی(همه)</label>');
         $("#"+"pvisible_items1"+(event.target.id).split("qedit_button")[0]).html('<input checked type="radio" id="pprivate" name="pvisible" value="1"> <label style="display:inline;" for="pprivate">خصوصی</label>')
         $("#"+"pvisible_items2"+(event.target.id).split("qedit_button")[0]).html('<input type="radio" id="ppassword" name="pvisible" value="2"> <label  style="display:inline;" for="ppassword">رمزدار</label>');
         $("#"+"pvisible_items3"+(event.target.id).split("qedit_button")[0]).html('<div id="ppassword_div" > <label style="display:block;padding-top:5px;" id="ppassword_label"  for="ppass">رمز عبور:</label> ');
         $("#"+"pvisible_items4"+(event.target.id).split("qedit_button")[0]).html('<input  type="text" class="form-control" name="ppass" id="'+event.target.id.split("qedit_button")[0]+'ppass">');
       }
       else if($("#"+"pvisible_items0"+(event.target.id).split("qedit_button")[0]).html()==2)
       {
        $("#"+"pvisible_items0"+(event.target.id).split("qedit_button")[0]).html('<input  type="radio" id="ppublic" name="pvisible" value="0">&nbsp<label style="display:block" for="ppublic">عمومی(همه)</label>');
        $("#"+"pvisible_items1"+(event.target.id).split("qedit_button")[0]).html('<input  type="radio" id="pprivate" name="pvisible" value="1"> <label style="display:inline;" for="pprivate">خصوصی</label>')
        $("#"+"pvisible_items2"+(event.target.id).split("qedit_button")[0]).html('<input checked type="radio" id="ppassword" name="pvisible" value="2"> <label  style="display:inline;" for="ppassword">رمزدار</label>');
        $("#"+"pvisible_items3"+(event.target.id).split("qedit_button")[0]).html('<div id="ppassword_div" > <label style="display:block;padding-top:5px;" id="ppassword_label"  for="ppass">رمز عبور:</label> ');
        $("#"+"pvisible_items4"+(event.target.id).split("qedit_button")[0]).html('<input  type="text" class="form-control" name="ppass" id="'+event.target.id.split("qedit_button")[0]+'ppass">');
       }
       //set post comment status with checkbox
       if($("#"+"comment_status_items"+(event.target.id).split("qedit_button")[0]).html()==1)
       {
         $("#"+"comment_status_items"+(event.target.id).split("qedit_button")[0]).html('<input  checked type="checkbox" id="pcomment_status" name="pcomment_status">&nbsp<label style="display:inline" for="pcomment_status">پذیرفتن دیدگاه</label>');
       }
       else if($("#"+"comment_status_items"+(event.target.id).split("qedit_button")[0]).html()==0)
       {
         $("#"+"comment_status_items"+(event.target.id).split("qedit_button")[0]).html('<input type="checkbox" id="pcomment_status" name="pcomment_status">&nbsp<label style="display:inline" for="pcomment_status">پذیرفتن دیدگاه</label>');
       }
      if(event.target.id == "q_edit_send")
      {
        // alert(postId);
        //check post password not empty
        // if(($("#ppass").val()).trim() == "" && $("#ppassword").prop("checked"))
        // {
        //   alert("yes");
        //   event.preventDefault();;
        //   Addnoticeerror();
        //   $("#ppass_error").html(Error('رمزعبور پست نمی تواند خالی باشد!'));
        // }
        // else
        //   $("#ppass_error").html('');
        Urireplacer("#"+puriId);
        $("#"+ptitleId).val(Trimer($("#"+ptitleId).val()));
          // var data = 
          // {
          //   op:"q_edit",
          //   tableName:"posts",
          //   id:Trimer($("#id_hidden").val()),
          //   title: Trimer($("#ptitle").val()),
          //   uri: $("#puri").val()
          //   public:$('input[name="pvisible"]:checked').val(),
          //   comment:$('#pcomment_status').val();
          //   publish:$("#ppublish_date").text().trim()
          // )};
          // Ajax(data);
      }
      
    })
    var data = {op:"addpostcat",fieldValue:'set 1'};//Error onchange and unset session if user refresh page
    Ajax(data,"cats_result");
    //check post password not empty
    $("#ppass").on("change",function(){
      if(($("#ppass").val()).trim() == "")
      {
        Addnoticeerror("add_notice0");
        $("#ppass_error").html(Error('رمزعبور پست نمی تواند خالی باشد!'));
      }
      else
      {
        $("#ppass_error").html('');
      }
    })
    //if add new cat clicked
    $("#add_new_category_button").on("click",function(){
      var data = {op:"Checkavailability",tableName:"categories",fieldName:"ctitle",fieldValue:$("#ctitle").val().trim(),op2:'clicked'};
      Ajax(data,"ctitle_error");
    })
    $("#ctitle").on("change",function()//check cat name
    {
      if(($("#ctitle").val()).trim() == "")
        $("#ctitle_error").html(Error('نام دسته جدید نمی تواند خالی باشد!'));
      else
      {
        $("#ctitle_error").html('');
        $("#ctitle").val(Trimer($("#ctitle").val()));
        var data = {op:"Checkavailability",tableName:"categories",fieldName:"ctitle",fieldValue:$("#ctitle").val().trim()};
        Ajax(data,"ctitle_error");
      }
    }); 
    
    $(".ptitle").on("change",function()//check post title
    {
      ptitleId = $(this).attr('id');
      ptitleIdNumber = ptitleId.match(/\d+/);
      if(ptitleIdNumber==null)
        ptitleIdNumber=0;
      if(($("#"+ptitleId).val()).trim() == "")
      {
        Addnoticeerror("add_notice"+ptitleIdNumber);
        $("#ptitle_result"+ptitleIdNumber).html(Error('عنوان مطلب نمیتواند خالی باشد !'));
      }
      else
      {
        $("#ptitle_result"+ptitleIdNumber).html('');
        $("#"+ptitleId).val((Trimer($("#"+ptitleId).val())));
      }
    });
      $(".puri").on("change",function()//replace post uri
      {
        puriId = $(this).attr('id');
        puriIdNumber = puriId.match(/\d+/);
        if(puriIdNumber==null)
          puriIdNumber=0;
        Urireplacer("#"+puriId);
        var data = {op:"Checkavailability",tableName:"posts",fieldName:"puri",fieldValue:$("#"+puriId).val().trim()};
        Ajax(data,"puri_result"+puriIdNumber);
      }); 
    $("#add,#edit").on("click",function(event)//check all field
    {
      //check post password not empty
      if(event.target.id=="add")
      {
        if(($("#ppass").val()).trim() == "" && $("#ppassword").prop("checked"))
        {
          event.preventDefault();;
          Addnoticeerror("add_notice0");
          $("#ppass_error").html(Error('رمزعبور پست نمی تواند خالی باشد!'));
        }
        else
          $("#ppass_error").html('');
      }
      Urireplacer("#puri0");
      $("#ptitle0").val(Trimer($("#ptitle0").val()));
      if(($("#ptitle0").val()).trim() == "")
      {
       event.preventDefault();
       Addnoticeerror("add_notice0");
       $("#ptitle_result0").html(Error('عنوان مطلب نمیتواند خالی باشد !'));
     }
     else
     {
        // var data = {op:"Checkavailability",tableName:"posts",fieldName:"puri",fieldValue:$("#puri0").val().trim()};
        // Ajax(data,"puri_result0");
        if($("#ppublish_date0").text().trim() !== "پس از درج مطلب")//if user add publishdate
        {
          if($("#date_error0:not(:empty)") && $("#date_accept0").prop('checked') == false)// if checkbox publishdate
          {
            event.preventDefault();
            Addnoticeerror("add_notice0");
          }
           else if($('#date_error0').is(':empty'))//if checkbox checked
           {
            var data = {op:"Checkdate",fieldName:"ppublish_date",fieldValue:$("#ppublish_date0").text().trim()};
            Ajax(data,"date_error0");
          }
        }
      }
    })

        $("#cats_form").slideDown(function()//list cats
        {
          var data = {op:"Samplelist",tableName:"categories"};
          Ajax(data,"post_category_items","","خطایی در دسترسی به دسته ها رخ داد( کد خطا: L1)!");
        });
        //slidup 
        $(".slideup").slideUp();
        $("#ttitle_search").on("keydown",function(event)// if in tag search enter clicked
        {
          if(event.keyCode == 13) 
          {
            if($("#ttitle_search").val().trim() != "")
            {
                 var data = {op:"Samplelist",tableName:"tags",fieldValue:$("#ttitle_search").val()};//add tag to post(Green)
                 Ajax(data,"spinner");
                 var data = {op:"Search",tableName:"tags",fieldValue:$("#ttitle_search").val()};//check tag new
                 Ajax(data,"spinner");
                 $("#ttitle_search").val('');
               }
               else
                $("#ttitle_search").val('');
            }
          });
        $(".ppublish_date_now").on("click",function()//if click on now publish date
        {
          $("#date_error"+ppublishIdNumber).html('');
          $('#ppublish_date_now'+ppublishIdNumber).attr('type','hidden');
          $("#ppublish_date"+ppublishIdNumber).text('پس از درج مطلب');
        })
        $(".ppublish_date").on("change",function()//if onchagne publish date
        {
          ppublishId = $(this).attr('id');
          ppublishIdNumber = ppublishId.match(/\d+/);
          if(ppublishIdNumber==null)
            ppublishIdNumber=0;
          var data = {op:"Checkdate",fieldName:"ppublish_date",fieldValue:$("#"+ppublishId).text().trim()};
          Ajax(data,"date_error"+ppublishIdNumber);
        })
        //PersianDateTimePicker plugin
        if(checkboxs.length==0)
        {
            $("#ppublish_date0").MdPersianDateTimePicker({
              targetTextSelector:'#ppublish_date0',//place of show date
              fromDate: true,
              enableTimePicker: true,
              groupId: 'rangeSelector1',
              dateFormat: "yyyy-MM-dd HH:mm:ss",// format of date
              textFormat: "yyyy-MM-dd HH:mm:ss",
            });
        }
        else
        {
          for(var i=0;i<=checkboxs.length-1;i++)
          {
            $("#ppublish_date"+checkboxs[i]).MdPersianDateTimePicker({
              targetTextSelector:'#ppublish_date'+checkboxs[i]+'',//place of show date
              fromDate: true,
              enableTimePicker: true,
              groupId: 'rangeSelector1',
              dateFormat: "yyyy-MM-dd HH:mm:ss",// format of date
              textFormat: "yyyy-MM-dd HH:mm:ss",
            });
          }
        }
      $(window).on("keydown",function(event)//cancel form send by enter
      {
        if(event.keyCode == 13) 
          event.preventDefault();
      });
      $("#usertags").on("click",function(event)//if user tag clicked!
      {
        if((event.target.id).search("close")!==-1)//if X clicked!then delete from post tag session
          var res = (event.target.id).split("_");
        var data = {op:"Search",tableName:"tags",fieldValue:res[1],op2:'Delete'};
        Ajax(data,"spinner");
      })

        $("#ttitle_search").on("keyup",function(){//checck user not add null
          if($("#ttitle_search").val().trim()=="")
          {
            $("#tag_result").html('');
            $("#ttitle_search").val('');
            event.preventDefault();;
          }
          else
          {
            $("#tag_result").html('');
            var data = {op:"Search",tableName:"tags",fieldValue:$("#ttitle_search").val().trim(),op3:"Search"};
            Ajax(data,"spinner");//list of tag(Blue)
          }
        });
        $("#cat_search").on("keyup",function(){// if search cat
          if($("#cat_search").val().trim()=="")
          {
            var data = {op:"Samplelist",tableName:"categories"};
            Ajax(data,"post_category_items");//list of cat
          }
          else
          {
            var data = {op:"Search",tableName:"categories",fieldName:"ctitle",fieldValue:$("#cat_search").val().trim()};
            Ajax(data,"post_category_items");//list of cat result search
          }
      });
      $("#tag_result").on("click",function(event)//if result tag clicked!(BLUE)
      {
        var data = {op:"Search",tableName:"tags",fieldValue:$("#tag_result").find("#"+event.target.id).html().trim()};
        Ajax(data,"spinner");//add to session post tag
        $("#tag_result").html('');
        $("#ttitle_search").val('');
      })
      
      $("#post_tag_label").click(function()
      {
        $("#post_tag_items").slideToggle();
      });
      $("#post_category_label").click(function()
      {
       $("#cats_form").slideToggle();
       $("#form_add_category").slideUp();
     });
      $("#add_category_button").click(function()
      {
        var data = {op:"Samplelist",tableName:"categories"};
        Ajax(data,"post_category_items_select","","خطایی در دسترسی به دسته ها رخ داد( کد خطا: L1)!");
        $("#form_add_category").slideToggle();
      });
      $("#post_publish_date_label").click(function()
      {
        $("#post_publish_date_items").slideToggle();
      });
      $("#comment_status_label").click(function()
      {
        $("#comment_status_items").slideToggle();
      });
      $("#pvisible").click(function()
      {
        $("#pvisible_items").slideToggle();
      });
      $( "#ppassword" ).on( "click", function() 
      {
        $("#ppassword_div").slideDown();
      });
      $( "#ppublic" ).on( "click", function() 
      {
        $("#ppassword_div").slideUp();
      });
      $( "#pprivate" ).on( "click", function() 
      {
        $("#ppassword_div").slideUp();
      });
      $('.cats').change(function()//if cat checkbox clicked! 
      {
        if($(this).is(":checked")) 
        {
          var data = {op:"addpostcat",fieldValue:$(this).attr('name')};
            Ajax(data,"cats_result");//add to session
          }
          else
          {
            var data = {op:"addpostcat",fieldValue:$(this).attr('name'),op2:"deletepostcat"};
            Ajax(data,"cats_result");//if checkbox unchecked
          }
        });
      if($("#edit").attr('id')=="edit")
      {
        $("#ppass_msg").html('در صورت خالی بودن از رمز قبلی استفاده خواهد شد.');
      }
    });