<?php
require_once 'main.php';//Security Check
require_once 'header.php';
// echo "<script>alert('yes')</script>";
//If JavaScript Disabled in Browser
echo "<noscript>افزودن پست نیازمند جاوااسکریپت است لطفا جاوااسکریپت را در مرورگر خود فعال کنید.</noscript>";
//Get Operation
switch ($_GET['op'])
{
	//Add Operation
	case 'add':
	{
		$form_title = $_ADD_POST;//Form Title From Language File
		//Post Variable(Entity of posttable in Database)
		$ptitle = "";
		$pbody = "";
		$puri = "";
		$psummary = "";
		$ppublic = "0";
		$pimage = "";
		$dir_temp_name = "";//Temp name for File Upload
		$pCommentStatus = "1";//Comment Status Post
		$pcomments = "";
		$upload_dir = '../images/featured/';// Directory File Upload
		//Update Post From postId
		if(isset($_GET['pid']))
		{
			$recordInfo = DB::ListRecords('*','posts',$where ='WHERE pid=:pid',$whereArray = array('pid'=>$_GET['pid']),$order='',$limit='');//Information of Post(for value of Field)
			$recordInfo = $recordInfo[0];
		}
		//Delete Image Posted in Update Post
		if(isset($_POST['del_img']))
		{
			del($upload_dir.$_GET['pid']);//Delete Image from Directory
		}
		//Check Image Upload
		if(!empty($_FILES['pimage']['name']))//If file Upload Not null
		{
			$dir_temp_name = substr(time(), -7);//Create temp-name from Current Time
			if(mkdir($upload_dir.$dir_temp_name))//If Create Folder Success
			{
				$pimage = basename($_FILES['pimage']['name']);//Get Orginial name of Image
				$whitelist = array('jpg','png','gif','svg','webp');	// list of Extention Valid image
				$whitelist_mime = array('image/gif','image/jpeg','image/png','image/svg+xml','image/webp');//list of MIME Type Valid image	
				//Get Image Extention
				$ext = explode('.',$pimage);
				$ext = $ext[count($ext)-1];
				$pimage = md5(uniqid().$pimage ).'.'.$ext;
				$whitelist = array('jpg','png');// list of Extention Valid image
				$whitelist_mime = array('image/jpeg','image/png');//list of MIME Type Valid image	
				// Where are we going to be writing to? 
				$temp_file = ((ini_get('upload_tmp_dir') == '')?(sys_get_temp_dir()):(ini_get('upload_tmp_dir'))); 
				$temp_file .= DIRECTORY_SEPARATOR.$pimage;
				//Check Extentions Valid Or Invalid
				if(in_array($ext,$whitelist))
				{
					$imageinfo = getimagesize($_FILES['pimage']['tmp_name']);//Get Information Image
					if(in_array($imageinfo['mime'], $whitelist_mime))//Check MIME Type Valid Or Invalid
					{
						if($imageinfo['mime'] == 'image/jpeg') 
						{
							$img = imagecreatefromjpeg($_FILES['pimage']['tmp_name']);// jpeg image create
							imagejpeg($img,$temp_file,100);// Output a jpeg image transfer to temp_file
						}
						else if($imageinfo['mime'] == 'image/png')
						{
							$img = imagecreatefrompng($_FILES['pimage']['tmp_name']);// png image create
							imagepng($img,$temp_file,9);// Output a png image transfer to temp_file
						}
						imagedestroy($img);
						// Can we move the file to the web root from the temp folder? 
						if(rename($temp_file,(getcwd().DIRECTORY_SEPARATOR.$upload_dir.$dir_temp_name.'/'. $pimage))) 
						{
							echo '<pre>succesfully uploaded!</pre>'; 
						}
						else
							$error  = $_PROBLEM_IN_UPLOADING_FEATURING_IMAGE;// Error In uploading File
						// Delete any temp files 
						if(file_exists($temp_file)) 
							unlink($temp_file);
					}
					else
						$error = "نوع فایل تصویرشاخص مجازنیست";
				}
				else
					$error = "پسوندتصویرشاخص مجازنیست.پسوندهای مجاز:".implode(',',$whitelist);

			}
			//If Error set
			if(!empty($error))
				rmdir($temp_file);//Remove Empty Folder
		}
		//Add Post
		if(isset($_POST['add']))
		{
			//Check Publish Date
			if(isset($_SESSION['PUBLISHDATE']))
			{
				if($_SESSION['PUBLISHDATE'] == 'ppublish_dateEmpty' || $_SESSION['PUBLISHDATE'] == 'ppublish_dateInvalid')//if publish date empty or Invalid
				$error = "تاریخ انتشار انتخاب شده نامعتبر است";
				else if(strpos($_SESSION['PUBLISHDATE'],'ppublish_datelt')!==false)// if publish date less than current date
				{
					//check date_accept
					if(empty($_POST['date_accept']))
					{
						$error = "تاریخ انتشارمطلب انتخاب شده از تاریخ فعلی سیستم کوچکتر است.لطفا تاریخ انتخاب شده را تایید کنید";
					}
					else
					{
						//Get publish date
						$str = explode("  ",$_SESSION['PUBLISHDATE']);
						$ppublish_date = $str[1];
					}
				}
				else if(strpos($_SESSION['PUBLISHDATE'],'ppublish_dategt')!==false)// if publish date greater than current date
				{
					//Get publish date
					$str = explode("  ",$_SESSION['PUBLISHDATE']);
						$ppublish_date = $str[1];
				}
			}
			else
				$ppublish_date = date("Y-m-d H:i:s");//Get current date for publishdate
			//Check Comment Status
			if(empty($_POST['pcomment_status']))
				$pCommentStatus = '0';
			//Get Post Visibility
			$ppublic = $_POST['pvisible'];
			//Get Post Title
			$ptitle = $_POST['ptitle'];
			//Check Post Title is empty
			if(trim($ptitle)=="")
				$error = "عنوان مطلب نمی‌تواند خالی باشد";
			//Get Post body
			$pbody = $_POST['pbody'];
			//Get Post uri
			$puri = $_POST['puri'];
			//Check Post uri
			if(isset($_SESSION['URI']))
			{
				if($_SESSION['URI'] == 'puriEmpty')//If Post uri Empty
					$error = "نامک مطلب نمی تواند خالی باشد";
				elseif($_SESSION['URI'] == 'puriInvalid')//If Post uri Invalid
					$error = "نامک وارد شده نامعتبر است";
				elseif($_SESSION['URI'] == 'Duplicate')//If Post uri Duplicate
					$error = "نامک وارد شده تکراری است";
			}
			else
				$error = "خطایی در بخش نامک رخ داده است! امکان افزودن مطلب وجود ندارد";	
			//Get Post Password Visibility(Option)
			$ppassword = $_POST['ppass'];
			//Get Post Comment
			$pcomments = $_POST['pcomments'];
			//Get Post Summary
			$psummary = $_POST['psummary'];
			//Get parameter for add to post table in database
			$params=array(
				'uid'=>$userInfo[0]['uid'],
				'ptitle' =>trim(Trimes($ptitle)),
				'pbody' =>$pbody,
				'psummary' =>$psummary,
				'ppublic' =>$ppublic,
				'ppassword'=>md5($ppassword),
				'pcomment_status'=>$pCommentStatus,
				'puri'=>trim(Trimes($puri)),
				'pimage'=>$pimage,
				'ppublish_date'=>$ppublish_date,
				'padd_date'=>date("Y-m-d H:i:s"),
				'pmod_date'=>date("Y-m-d H:i:s"),
				'pcomments'=>trim($pcomments)
			);
			//Check post Visibility
			if($ppublic=='2')
			{
				//Check post Password Empty
				if(trim($ppassword)=="")
					$error = "یک کلمه عبور مناسب برای پست خود انتخاب کنید";
			}
			else
				unset($params['ppassword']);//Delete parameter for add to post table in database 
			if(empty($error))
				$last_id = DB::AddRecord($params,'posts');//Add Record To Post Table in database
				if(!empty($last_id))//If Error not set in Add Record To Post Table in database
				{
						$last_id_array = [];//For Work With last_id
						array_push($last_id_array,$last_id);//push last_id to array
						$params = [];
						//Add newtags to Tag Table in database
							for( $i = 0;$i<=(count($_SESSION['newtags']))-1;$i++)
							{
								$params = array(
								'uid'=>$userInfo[0]['uid'],
								'ttitle' =>$_SESSION['newtags'][$i],
								'turi' =>$_SESSION['newtags'][$i]
								);
								$addUserTags = DB::AddRecord($params,'tags');
							}
							if(!empty($_SESSION['newtags']) || empty($params))
							{
								
								$TagIdsArray = [];
								//List tags from tags Table in database
								for( $j = 0;$j<=(count($_SESSION['posttags']))-1;$j++)
								{
									$where = 'WHERE ttitle='."'".$_SESSION['posttags'][$j]."'";
									$TagIdsVar = DB::ListRecords("tid","tags",$where);
									array_push($TagIdsArray,$TagIdsVar);
								}
								$params = [];
								//Add tags to posttag Table in database
								foreach ($TagIdsArray as $TagIdsArray2) 
								{
									$params = array(
										'tid'=>$TagIdsArray2[0]['tid'],
										'pid'=>$last_id,
									);
									$last_id_tag_post = DB::AddRecord($params,'tag_post');
								}
								//if set error in Add tags to posttag Table in database
								if(empty($last_id_tag_post) && !empty($params))
								{
									$error = "مشکلی در درج پست رخ داده است( کد خطا: A14)";
									$delResult = DB::DeleteRecords($last_id_array,'pid','posts');//Delete Post Because error in Add tags to posttag Table in database
									if(empty($delResult))// Can not Delete Post in post Table in database
										$error .= "<br>مشکلی در درج پست رخ داده است( کد خطا: D1410)";
								}
							}
							else
							{
								$error = "مشکلی در درج پست رخ داده است( کد خطا: A13)";
								$delResult = DB::DeleteRecords($last_id_array,'pid','posts');
								//Delete Post Because error in Add tags to posttag Table in database
								if(empty($delResult))// Can not Delete Post in post Table in database
									$error .= "<br>مشکلی در درج پست رخ داده است( کد خطا: D1310)";
							}
					if(empty($error))// if error not set
					{
						if(count($_SESSION['addpostcat']) == 0)// if Nothing Cat Selected
						{
							$_SESSION['addpostcat'][] = 1;
						}
						//Add categorypost to category_post Table in database
						foreach ($_SESSION['addpostcat'] as $value) 
						{
							$params=array(
								'cid'=>$value,
								'pid'=>$last_id,
							);
							$last_id_category_post = DB::AddRecord($params,'category_post');
							//if can not add categorypost to category_post Table in database
							if(empty($last_id_category_post))
							{
								$error = "مشکلی در درج پست رخ داده است( کد خطا: A2)";
								$delResult = DB::DeleteRecords($last_id_array,'pid','posts');
								//Delete Post Because error in Add categorypost to category_post Table in database
								if(empty($delResult))// Can not Delete Post in post Table in database
									$error .= "<br>مشکلی در درج پست رخ داده است( کد خطا: D10)";
							}
							else // if allthing Ok
							{
								$success = "درج پست با موفقیت انجام شد ";
							}
						}
					}
					if(!empty($_FILES['pimage']))// if user Select an image
						rename($upload_dir.$dir_temp_name,$upload_dir.$last_id);//Rename temp-name with lastid(post)
				}
				// else
				// 	$error = "مشکلی در افزودن پست رخ داد(کدخطا:A10)";
		}
		//if user want to update post(show edit fields)
		else if(isset($_GET['pid']) && !isset($_POST['edit']))
		{
			$form_title = $_EDIT_POST;//get form title from language file
			//set value fields(post) 
			$uid = $recordInfo['uid'];
			$ptitle = $recordInfo['ptitle'];
			$pbody = $recordInfo['pbody'];
			$puri = $recordInfo['puri'];
			$pimage = $recordInfo['pimage'];
			$pcomments = $recordInfo['pcomments'];
		}
		//if user submit update clicked
		else if(isset($_GET['pid']) && isset($_POST['edit']))
		{
			$form_title = $_EDIT_POST;//get form title from language file
			$uid = $_POST['uid'];
			$ptitle = $_POST['ptitle'];
			$pbody = $_POST['pbody'];
			$puri = $_POST['puri'];
			$pcomments = $_POST['pcomments'];
			//set parameter for update post
			$params=array(
				'uid' =>$uid,
				'ptitle' =>$ptitle,
				'pbody' =>$pbody,
				'puri'=>$puri,
				'pimage'=>$pimage,
				'pmod_date'=>date("Y-m-d H:i:s"),
				'pcomments'=>$pcomments,
				'pid'=>$_GET['pid']
			);
			// if(empty($error))
				//update post Query
				if(DB::UpdateRecord('posts',$params,'WHERE pid=:pid'))
				{
					$success = "یک رکورد با موفقیت ویرایش شد";

					if(!empty($_FILES['pimage']))// if selected an image in update post
					{
						if(file_exists($upload_dir.$_GET['pid']))// check file Exists 
							del($upload_dir.$_GET['pid']);//delete  fill Folder 
						rename($upload_dir.$dir_temp_name,$upload_dir.$_GET['pid']);//rename tmp-name to postid name
					}	
				}
				else// if cant update post
				{
					$error = "مشکلی در ویرایش رکورد رخ داد";
				}
		}
		if(!empty($success))// if post successfully  add or update to database
		{
			//because prev value empty
			unset($_SESSION['URI']);
			unset($_SESSION['PUBLISHDATE']);
			unset($_SESSION['newtags']);
			unset($_SESSION['posttags']);
			unset($_SESSION['tags']);
			unset($_SESSION['addpostcat']);
			Success($success);
		}
		if(!empty($error))// if post Not successfully  add or update to database
		{
			//because prev value empty
			unset($_SESSION['URI']);
			unset($_SESSION['addpostcat']);
			if($error !== "تاریخ انتشارمطلب انتخاب شده از تاریخ فعلی سیستم کوچکتر است.لطفا تاریخ انتخاب شده را تایید کنید")// publish date require checkbox checked
					unset($_SESSION['PUBLISHDATE']);
			Error($error);
		}
		//add or update or delete or list form(panel)
		echo '
		<h1><span class="badge badge-primary">'.$form_title.'</span></h1>
		<div class="d-flex align-content-center flex-wrap">
		<div style="width:40%;">
		<form id="add_post_form" action = "" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="ptitle">عنوان مطلب:</label>
				<input type="text" class="form-control ptitle" name="ptitle" id="ptitle0" value="'.$ptitle.'" autofocus>
				<div id="ptitle_result0"></div>
			</div>
			<div class="form-group">
				<label for="puri">نامک مطلب:</label>
				<input type="text" class="form-control puri" name="puri" id="puri0" dir="ltr" value="'.$puri.'">
				<div id="puri_result0"></div>
				<div id="puri_Rules" class="text-danger">موارد مجاز شامل حروف لاتین، اعداد، خط تیره می باشد.</div>
			</div>
			<div class="form-group">
				<label for="pbody">شرح مطلب:</label>
				<textarea class="form-control editor" name="pbody" id="pbody">'.$pbody.'</textarea>
			</div>
			<div class="form-group">
				<label for="pabstract">چکیده مطلب:</label>
				<textarea class="form-control editor" name="pabstract" id="pabstract"></textarea>
			</div>
			<div class="form-group">
				<label for="pcomments">توضیحات:</label>
				<textarea class="form-control"  name="pcomments" id="pcomments">'.$pcomments.'</textarea>
			</div>
			<div class="form-group">
				<label for="pimage">تصویر شاخص: </label><br>';
				//for update show image
		if(isset($_GET['pid']) && !empty($pimage) && file_exists($upload_dir.$_GET['pid'].'/'.$pimage))
		{
			echo '<img src = "'.$upload_dir.$_GET['pid'].'/'.$pimage.'" width = "200px"><br>
			<label><input type = "checkbox" name="del_img" id="del_img" value="1">حذف تصویر</label><br>';
		}
		else
			//show default image with input upload file
			echo '<img src = "'.$upload_dir.'featured.png'.'" width = "200px"><br>';
				echo'
				<input type="file" name="pimage" id="pimage">
				<div id="imgupload_res"></div>
			</div>';
		if(isset($_GET['pid']))
		{
			$adminsList = DB::ListRecords("*","users",$where = "WHERE utype=1");
			echo '
			<div class="form-group">
				<label for="uid">نویسنده :</label>
				<select name="uid" class="form-control">';
				//another user update post by another user adn show combo box
			foreach ($adminsList as $adminInfo2) 
			{
				echo '<option '.(adminInfo2['uid'] ==$uid?'selected':'').' value="'.$adminInfo2['uid'].'">'.$adminInfo2['uusername'].'('.$adminInfo2['ufname'].' '.$adminInfo2['ulname'].')</option>';
			}
			echo '
				</select>
			</div>	
			';
		}
		//name button add or update
		echo '
			<input id="'.(isset($_GET['pid'])?'edit':'add').'" name="'.(isset($_GET['pid'])?'edit':'add').'" type="submit" class="btn btn-success" value="'.(isset($_GET['pid'])?'ویرایش':'افزودن').'">
			<div id="add_notice0"></div>
		</div>
		<div style="width:20%;"></div>
		<div style="width:40%;">
			<div class="form-group" class="form-control">
				<label id="pvisible" for="pvisible">وضعیت مشاهده مطلب <i class="fa fa-sort-down"></i></label>
				<div class="form-group slideup" id="pvisible_items" >
					<input checked type="radio" id="ppublic" name="pvisible" value="0">
					<label for="ppublic">عمومی(همه)</label><br>
					<input type="radio" id="pprivate" name="pvisible" value="1">
					<label for="pprivate">خصوصی</label><br>
					<input type="radio" id="ppassword" name="pvisible" value="2">
					<label for="ppassword">رمزدار</label><br>
					<div id="ppassword_div" class="slideup">
						<label id="ppassword_label"  for="ppass">رمز عبور:</label><br>
						<input  type="text" class="form-control" name="ppass" id="ppass">
						<span id="ppass_msg"></span>
						<span id="ppass_error"></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label id="comment_status_label" for="comment_status_label">وضعیت دیدگاه مطلب<i class="fa fa-sort-down"></i></label>
				<div id="comment_status_items" class="slideup">
					<input  checked type="checkbox" id="pcomment_status" name="pcomment_status">
					<label for="pcomment_status">پذیرفتن دیدگاه</label><br>
				</div>
			</div>
			<div class="form-group">
				<label id="post_publish_date_label" for="post_publish_date_label"> زمان انتشار مطلب<i class="fa fa-sort-down"></i></label>
				<div id="post_publish_date_items" class="slideup">
					 <button id="ppublish_date0" name="ppublish_date" type="button" class="btn btn-primary btn-sm ppublish_date">پس از درج مطلب</button>
					 <input  id="ppublish_date_now0" name="ppublish_date_now"  type="hidden" class="btn btn-secondary btn-sm ppublish_date_now" value="پس از درج مطلب">
					 <div id="date_error0"></div>
				</div>
			</div>
				<label id="post_category_label" for="post_category_label">دسته ها<i class="fa fa-sort-down"></i></label><br>
			<div id="cats_form" class="form-group slideup">
				<label id="cat_search_label" for="cat_search">جستجوی دسته‌ها:</label><br>
				<input class="form-control" type="text" id="cat_search" name="cat_search"><br>
				<span id="cats_result"></span><br>
				<span id="error_load_cat"></span>
				<ul style="list-style-type: none" id="post_category_items" class="form-group" class="form-control">
				</ul>
					<button id="add_category_button" name="add_category_button" type="button" class="btn btn-secondary btn-sm">افزودن دسته جدید</button>
					<div class="form-group"  id="form_add_category">
					 <label for="ctitle">نام دسته جدید: </label><br>
					 <input class="form-control" type="text" id="ctitle" name="ctitle">
					 <span id="ctitle_error"></span><br>
					 <label for="cparent">دسته مادر: </label><br>
					 <div id="parent_div">
					 <select id = "post_category_items_select" class="form-control">
					 </select>
					 <br><button id="add_new_category_button" name="add_category_button" type="button" class="btn btn-primary btn-sm">افزودن دسته جدید</button><br>
					 <span id="add_new_category_result"></span><br>
					 </div> 
				</div>
			</div>
			<div class="form-group">
				<label id="post_tag_label" for="post_tag_label">برچسب ها<i class="fa fa-sort-down"></i></label>
				<div id="post_tag_items" class="slideup">
					<label for="ttitle_search">افزودن برچسب: </label><br>
						<input class="form-control" style="margin-bottom:5px;" class="form-control" type="text" id="ttitle_search" name="ttitle_search">
						<div id="ttitle_Rules" class="text-danger">برای افزودن برچسب کلید Enter را بزنید.</div>
						<div id="usertags">
						</div>
						<div id="tag_result"></div>
				</div>
			</div>
		</div>
		</form>';
		}
	break;
	//list posts
	case 'list':
		if(!empty($_SESSION['del']))// if post deleted
		{
			Success("رکورد(های) موردنظرباموفقیت حذف شد");
			unset($_SESSION['deleteposts']);
			$_SESSION['del']="";
		}
		ListPosts(1,1);// list posts
	break;
	case 'del':
		if(isset($_GET['pid']))
		{
			//one post for delete
			if(DB::DeleteRecords(array($_GET['pid']),'pid','posts')>0)//>0 can delete post
			{
				$_SESSION['del'] = $_GET['pid'];
				echo'<script>window.location.href="?op=list"</script>';//go to post list
			}
			else//set error
				Error('مشکلی درحذف رکورد(ها) به وجودآمد');
		}
		elseif(isset($_GET['multipledelete']))
		{
			//multiple delete
			if(DB::DeleteRecords($_SESSION['deleteposts'],'pid','posts')>0)//>0 can delete post
			{
				$_SESSION['del'] = $_SESSION['deleteposts'];
				echo'<script>window.location.href="?op=list"</script>';//go to post list
			}
			else//set error
				Error('مشکلی درحذف رکورد(ها) به وجودآمد');
		}
	break;
}
require_once 'footer.php';
?>