<?php
require_once 'main.php';//Security Check
require_once '../includes/functions.php';//Include Public Functions
require_once 'header.php';
//Check Admin Login
if($isLoggedIn)
{
	require_once 'header.php';
	echo Success("مدیرکل گرامی به پنل مدیریت پرسفا خوش آمدید",1);
}
//Admin Not Login
else
{
	header('location: ../login.php');// Go to Login
}
require_once 'footer.php';
?>