<?php
echo 
'<!DOCTYPE html>
<html>
	<head>';
		
echo '
		<link rel="stylesheet" href="../admin/themes/default/css/bootstrap.min.css" type="text/css" media="all">
		<link rel="stylesheet" href="../admin/themes/default/css/style.css" type="text/css" media="all">';
echo '
		<title>Pressfa Installation - نصب سیستم پرسفا</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Testa">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<script type="text/javascript" src="../js/scripts.js"></script>
		<style>
		#logo {
		    animation: 2s ease 0s normal none 1 running logoPlay;
		}
		@keyframes logoPlay
		{
			from{margin-right:150px; opacity:0;}
			to{margin-right:0px; opacity:1;}
		}
		</style>
	</head>
	<body dir="rtl">
	';
echo '
	<div class="container" id="container" style="width:900px;">
		<div class="jumbotron">
		<form class="form-vertical" id="installTesta" method="post" action="">';
require_once('../config.php');
include_once('../includes/class.database.php');
// include_once('../include/class.settings.php');
if($link!=0)
{
	echo '
	<h3 style="color:red;">
	سیستم پیش از این نصب شده است!<br>
	اگر شما مدیر سیستم هستید، لطفاً این پوشه (یعنی پوشه install) را برای حفظ امنیت سیستم، پاک نمایید.</h3>
	<br><div style="direction:ltr; text-align:left; font-size:15pt; color:red;">System has already been installed!!</div>';
}
else
{
	if(isset($_POST['install']))
	{
		$settings = new ManageSettings();
		$site_key = trim($_POST['site_key']);
		$dbhost = $_POST['host'];
		$dbname = $_POST['dbname'];
		$dbuname = $_POST['dbuname'];
		$dbpass = $_POST['dbpass'];
		$prefix = $_POST['prefix'];
		$nomra_prefix = $_POST['prefix'];
		$language = $_POST['language'];
		$dir = $_POST['dir'];
		$system_title = $_POST['system_title'];
		$admin_username = $_POST['admin_username'];
		$admin_password = md5($_POST['admin_password']);
		$domain = ($_SERVER['SERVER_NAME']=="localhost"?"localhost":get_domain('http://'.$_SERVER['SERVER_NAME']));
		$site_key_error = 'کلید سایت معتبر نمی‌باشد. <a href="http://testa.cc">لطفاً با ما تماس بگیرید</a>. (Site Key is invalid. Please cotact us)<br>';
		$site_key_return = $settings->CheckSiteKey($site_key);
		$prefix = $_POST['prefix'].'_';
		$nomra_prefix = (!empty($_POST['nomra_prefix'])?$_POST['nomra_prefix'].'_':$prefix);

		if(empty($site_key) || empty($dbhost) || empty($dbname) || empty($dbuname) || empty($dbpass) || empty($prefix) || empty($language) || empty($dir) || empty($system_title) || empty($admin_username) || empty($admin_password))
			$error = "پر کردن تمام فیلدها الزامی است! (All fields are required)<br>";
		elseif ($site_key_return!=-1)
			$error = 'Error #'.$site_key_return.': '.$site_key_error;
		elseif ($db_connection->connect()=="0")
			$error = "اطلاعات پایگاه داده غلط است. ارتباط با بانک میسر نشد! (Database information are incorrect)";
		elseif ($nomra_prefix!=$prefix)
		{
			$link = $db_connection->connect();
			$result = $link->query("SELECT 1 FROM `".$nomra_prefix."settings` LIMIT 1");
			if(!$result)
				$error = "پیشوند نمرا اشتباه وارد شده و یا نام دیتابیس و نام کاربری و رمز عبوری که وارد کرده‌اید همان‌هایی نیست که در نمرا به کار رفته. (برای اتصال پرسفابه نمرا، این اطلاعات باید عیناً مانند نمرا باشند. می‌توانید از داخل فایل config.php در نمرا این اطلاعات را به دست آورید).<br>(Nomra \$table_prefix is incorrect) ";
		}
		
		if(!isset($error)) // Now install Testa
		{
			$handle=fopen('../config.php', 'w+');
			$data = "<?php\n\$allow_signup=1;\n\n\$dbhost='".$dbhost."';\n\$dbname='".$dbname."';\n\$dbuname='".$dbuname."';\n\$dbpass='".$dbpass."';\n\$table_prefix='".$prefix."';\n\$admin_session_name='testa_admin';\n\$admin_password_session_name='testa_admin_pass';\n\$user_session_name='testa_user';\n\$user_password_session_name='testa_user_pass';\n\$page_limit=20;\n\$qnum_final=20;\ndefine(\"_IMAGE_SIZE\",200); // in KB\n\$site_key='$site_key';\n\$nomra_prefix='$nomra_prefix';\n\$cookie = 1;\n\$cookie_admin=0;\n\$captcha = 0;\n\$showDetailedRank = 1;\n\$export_num=100;\n?>";

			if (fwrite($handle, $data))
			{
				echo '<div class="alert alert-success">
								  فایل config.php با موفقیت ایجاد شد.
							  </div>';
				try {
					$link = $db_connection->connect();
					/*** echo a message saying we have connected ***/
					echo '<br>';
				
					/*** set the PDO error mode to exception ***/
					$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					/*** begin the transaction ***/
					$link->beginTransaction();
				
					/*** CREATE table statements ***/
					$link->exec("
					CREATE TABLE `".$prefix."admins` (
					  `aid` int(11) NOT NULL,
					  `ausername` varchar(100) COLLATE utf8mb4_persian_ci NOT NULL,
					  `apass` varchar(32) COLLATE utf8mb4_persian_ci NOT NULL,
					  `afname` varchar(40) COLLATE utf8mb4_persian_ci DEFAULT NULL,
					  `alname` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL,
					  `apic` varchar(10) COLLATE utf8mb4_persian_ci DEFAULT NULL,
					  `aemail` varchar(100) COLLATE utf8mb4_persian_ci DEFAULT NULL
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;
					");
					$link->exec("
					CREATE TABLE `".$prefix."categories` (
					  `cid` int(10) NOT NULL COMMENT 'شناسه دسته',
					  `uid` int(11) NOT NULL,
					  `ctitle` varchar(255) COLLATE utf8mb4_persian_ci NOT NULL COMMENT 'عنوان دسته',
					  `curi` varchar(255) COLLATE utf8mb4_persian_ci NOT NULL COMMENT 'نامک دسته',
					  `cparent` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'والد دسته'
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;
					");
					$link->exec("
					CREATE TABLE `".$prefix."galleries` (
					  `gid` int(11) NOT NULL,
					  `uid` int(11) NOT NULL,
					  `gtitle` varchar(255) COLLATE utf8mb4_persian_ci NOT NULL,
					  `guri` varchar(255) COLLATE utf8mb4_persian_ci NOT NULL,
					  `gcomments` text COLLATE utf8mb4_persian_ci,
					  `gadd_date` datetime NOT NULL,
					  `gmode_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_persian_ci;
					");

					/*** commit the transaction ***/
					$link->commit();

					/*** echo a message to say the database was created ***/
					$success = '
					<h3 style="direction:ltr; text-align:left;">Congratulations! Testa installed successfully ;)</h3><br><div style="direction:ltr; text-align:left;">Please delete the "install" folder for security reasons...</div><br>
					تبریک عرض می‌کنیم! پرسفا با موفقیت نصب شد!<br>
					لطفاً به بخش مدیریت سیستم وارد شوید و تنظیمات سیستم را کامل نمایید و طبق راهنمای تستا، از آن استفاده نمایید.<br>
					<a href="../admin"><strong>پنل مدیریت تستا</strong></a><br>
					<a href="../"><strong>پنل مربوط به کاربران</strong></a><br><br>
					
					<strong>راهنمای تستا</strong>:<br>
					<a href="http://help.testa.cc" target="_blank">http://help.testa.cc</a>	
					<br><br>
					<strong style="color:red;">توجه:</strong> 
					 لطفاً برای حفظ امنیت سیستم، پوشه install را پاک نمایید.
								
					';
					echo '<div class="alert alert-success">
					  '.$success.'
				  </div>';
				}
				catch(PDOException $e)
				{
					/*** roll back the transaction if we fail ***/
					$link->rollback();
				
					/*** echo the sql statement and error message ***/
					echo $e->getMessage();
				}
			}
			else
				echo '<div class="alert alert-danger">
				  مشکلی در ساختن فایل config.php به وجود آمد. لطفاً سطح دسترسی این فایل را روی 777 تنظیم نمایید.
			  </div>';
		}
		
	}
	if(isset($error) || !isset($_POST['install']))
	{

		if(isset($error))
		echo '<div class="alert alert-danger">
		  '.$error.'
	  </div>';
	
		echo'<center>
		<img src="../img/testa.png" id="logo">
		<h3 class="alert alert-warning"><b>سیستم نصب پرسفا</b></h3>
		</center>';
		echo 'به نظر می‌رسد سیستم پرسفاهنوز نصب نشده است! برای نصب این سیستم، اطلاعات خواسته شده را به طور کامل وارد نمایید...<br>
		<div style="direction:ltr; text-align:left;">
		It seems that Testa has not been installed yet. To install Testa, please fill out the below form...<br><br>
		</div>
		<a class="label label-danger" href="../copyright.php">کاربر گرامی، خواهشمندیم، پیش از هر چیز، با کلیک روی این متن، حق انتشار پرسفا را مطالعه بفرمایید.</a><br><br>
		';		
		
		echo '
		<table width="100%">
			<tr style="height:20px;" class="label-primary">
				<td width="30%" style="color:white;">
				<strong>سیستم امنیتی تستا</strong>:
				</td>
				<td width="40%">
				
				</td>
				<td width="30%" style="direction:ltr; color:white;">
				<strong>Testa Security System</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>کلید سایت</strong>:<br>
				<span class="small">کدی که هنگام خرید این نسخه از پرسفادریافت کرده‌اید، وارد نمایید.</span>
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['site_key'])?trim($_POST['site_key']):"").'" type="text" class="form-control input" autocomplete="off" id="site_key" name="site_key" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Site Key</strong>:<br>
				<span class="small">Please enter the Site Key which you received after buying Testa.</span>
				</td>
			</tr>
			<tr style="height:20px;" class="label-primary">
				<td width="30%" style="color:white;">
				<strong>اطلاعات پایگاه داده</strong>:
				</td>
				<td width="40%">
				
				</td>
				<td width="30%" style="direction:ltr; color:white;">
				<strong>Database Information</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>آدرس هاست</strong>:<br>
				<span class="small">توجه: اگر نمی‌دانید این فیلد به چه معناست، همان localhost رها کنید. در اکثر مواقع این عبارت صحیح است.</span>
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['host'])?$_POST['host']:"localhost").'" type="text" class="form-control input" autocomplete="off" id="host" name="host" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Host Address</strong>:<br>
				<span class="small">Leave it "localhost" if you do not know what it is.</span>
				</td>
			</tr>
			<tr style="background-color:#e3e3e3">
				<td width="30%">
				<strong>نام دیتابیس</strong>:
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['dbname'])?$_POST['dbname']:'').'" type="text" class="form-control input" autocomplete="off" id="dbname" name="dbname" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Database Name</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>نام کاربری دیتابیس</strong>:
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['dbuname'])?$_POST['dbuname']:'').'" type="text" class="form-control input" autocomplete="off" id="dbuname" name="dbuname" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Database Username</strong>:
				</td>
			</tr>
			<tr style="background-color:#e3e3e3">
				<td width="30%">
				<strong>رمز عبور دیتابیس</strong>:
				</td>
				<td width="40%">
				<input value="" type="password" class="form-control input" autocomplete="off" id="dbpass" name="dbpass" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Database Password</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>پیشوند (دلخواه) برای جداول دیتابیس</strong>:<br>
				<span class="small">یک پیشوند سه حرفی شامل کاراکترهای انگلیسی برای جداول انتخاب کنید.<br>مثال: tst یا aft یا acc و غیره<br>
				توجه: اگر روی این دیتابیس نمرا نیز نصب شده، باید پیشوند را متفاوت نسبت به پیشوند نمرا انتخاب کنید.
				</span>
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['prefix'])?$_POST['prefix']:'').'" type="text" class="form-control input" autocomplete="off" maxlength="3" size="3" id="prefix" name="prefix" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Tables\' Prefix</strong>:<br>
				<span class="small">Choose a 3-letter prefix containing English Alphabetic Letters.</span>
				</td>
			</tr>
			<tr style=" height:20px;" class="label-primary">
				<td width="30%" style="color:white;">
				<strong>اتصال به سیستم‌های دیگر ما</strong>:
				</td>
				<td width="40%">
				
				</td>
				<td width="30%" style="direction:ltr; color:white;">
				<strong>System Integration</strong>:
				</td>
			</tr>
			<tr style="background-color:#e3e3e3">
				<td width="30%">
				<strong>پیشوند جداول <a href="http://nomra.ir" target="_blank">سیستم نمرا</a></strong> <span class="small">(اختیاری)</span>:<br>
				<span class="small">توجه: اگر شما سیستم نمرا را پیش از این خریداری و نصب کرده‌اید و قصد یکپارچه‌سازی پرسفاو نمرا را دارید، پیشوند سه کاراکتری جداول نمرا را وارد نمایید.<br>
				در صورت عدم تمایل به اتصال این دو سیستم، این فیلد را خالی بگذارید. (<a href="http://yourl.ir/testa_nomra" target="_blank">اطلاعات بیشتر</a>)
				</span>
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['nomra_prefix'])?$_POST['nomra_prefix']:'').'" type="text" class="form-control input" autocomplete="off" id="nomra_prefix" name="nomra_prefix" maxlength="3" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Nomra3+ Tables\' Prefix</strong>:<br>
				<span class="small">You can find <a href="http://nomra.ir" target="_blank">Nomra3</a> $prefix in config.php in Nomra root folder.<br>
				Leave this feild blank if you do not need to integrate testa and nomra...</span>
				</td>
			</tr>
			
			<tr style=" height:20px;" class="label-primary">
				<td width="30%" style="color:white;">
				<strong>اطلاعات سیستم</strong>:
				</td>
				<td width="40%">
				
				</td>
				<td width="30%" style="direction:ltr; color:white;">
				<strong>System Information</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>زبان سیستم</strong>:
				</td>
				<td width="40%">
				<select class="form-control" name="language" style="width:150px; direction:ltr;">
							';
						$directory=opendir('../language');
					
						while (false != ($file=readdir($directory)))
						{
							if (strpos($file, '.php', 1) && strpos($file, 'custom')===false)
							{
								$rest=substr("$file", 0, -4);
								if(isset($_POST['language']) && $rest == $_POST['language'])
									echo ('<option value="' . $rest . '" selected="selected">' . ucfirst($rest) . '</option>');
								else
									echo ('<option value="' . $rest . '">' . ucfirst($rest) . '</option>');
							}
						}
					
						closedir ($directory);
						
						$dir_checked1 = $dir_checked2 ="";
						if(isset($_POST['dir']) && $_POST['dir']=="ltr")
							$dir_checked2 = 'checked="checked"';
						else
							$dir_checked1 = 'checked="checked"';
						
						echo '	</select>
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>System Language</strong>:
				</td>
			</tr>
			<tr style="background-color:#e3e3e3">
				<td width="30%">
				<strong>چینش زبان</strong>:
				</td>
				<td width="40%">
					<input type="radio" name="dir" id="dir1" value="rtl" '.$dir_checked1.'> <label for="dir1">راست به چپ</label> 
					<input type="radio" name="dir" id="dir2" value="ltr" '.$dir_checked2.'> <label for="dir2">چپ به راست</label> 
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Language Direction</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>عنوان سایت</strong>:
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['system_title'])?$_POST['system_title']:'').'" type="text" class="form-control input" autocomplete="off" id="system_title" name="system_title" style="direction:rtl;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>Website Title</strong>:
				</td>
			</tr>
			<tr style="height:20px;" class="label-primary">
				<td width="30%" style="color:white;">
				<strong>اطلاعات مدیر کل</strong>:
				</td>
				<td width="40%">
				
				</td>
				<td width="30%" style="direction:ltr; color:white;">
				<strong>God Admin Information</strong>:
				</td>
			</tr>
			<tr style="background-color:#e3e3e3">
				<td width="30%">
				<strong>نام کاربری مدیر کل</strong>:<br>
				<span class="small">مثال: Hamid یا Mojtaba و غیره<br>
				بهتر است کلمات واضحی مثل Admin نباشد</span>
				</td>
				<td width="40%">
				<input value="'.(isset($_POST['admin_username'])?$_POST['admin_username']:'').'" type="text" class="form-control input" autocomplete="off" id="admin_username" name="admin_username" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>God Admin Username</strong>:
				</td>
			</tr>
			<tr>
				<td width="30%">
				<strong>رمز عبور مدیر کل</strong>:<br>
				<span class="small">توجه: بهتر است امن باشد و در عین حال به راحتی به یاد آورید.<br>
				در صورتی که این رمز را فراموش کنید، فقط از طریق دیتابیس می‌توانید آن‌را بازگردانید.
				</span>
				</td>
				<td width="40%">
				<input value="" type="password" class="form-control input" autocomplete="off" id="admin_password" name="admin_password" style="direction:ltr;">
				</td>
				<td width="30%" style="direction:ltr;">
				<strong>God Admin Password</strong>:
				</td>
			</tr>
			
		</table>
		<br>
		<span style="color:red">توجه: پر کردن تمام فیلدها الزامی است.</span>
		<center><input type="submit" name="install" value="نصب تستا" class="btn btn-primary btn-large" ></center>
		
		';
	}

}
echo'
</form>
</p>
</div>
';
$isLogedIn=0;
include_once('../footer.php');
?>