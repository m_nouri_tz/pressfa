<?php
require_once '../includes/jdf.php';
// echo time();
// date_default_timezone_set("Asia/Tehran");
// echo date("Y-m-d H:i:s",1300000000);
// echo mktime(0,0,0,10,3,1975);
// echo date("Y-m-d H:i:s",strtotime("+1 year",strtotime("2020-09-13 14:38:15")));
// echo date("Y-m-d H:i:s",jmktime(12,41,0,7,6,1399,"UTC"));
// echo '<br>';
// echo gmmktime(4,10,0,9,24,2020);
// $date = new DateTime($time, new DateTimeZone($fromTz));
// $date->setTimezone(new DateTimeZone($toTz));
// $time= $date->format('Y-m-d H:i:s');
// echo jdate("Y-m-d H:i:s",strtotime("2020-09-26 20:48:31")).'<br>';
// echo jdate("Y-m-d H:i:s",strtotime("2020-09-26 20:48:31")).'<br>';
// echo jdate("Y-m-d H:i:s",strtotime("2020-09-26 20:48:31")).'<br>';
// echo jdate("Y-m-d H:i:s",strtotime("2020-09-26 20:48:31")).'<br>';
// echo jdate("Y-m-d H:i:s",strtotime("2020-09-26 20:48:31")).'<br>';
// echo jdate("Y-m-d H:i:s",strtotime("2020-09-26 20:48:31"));
// echo G2J(date());
function G2J($dt,$lang = 'english')
{
	date_default_timezone_set("UTC");
	$dt = jdate("Y-m-d H:i:s",strtotime($dt));
	if($lang == 'english')
		return $dt;
	else
		return tr_num($dt,'fa');
}
function J2G($dt,$lang = 'english')
{
	$dt = explode(" ", $dt);
	$d = $dt[0];
	$t = $dt[1];
	$d = explode("-", $d);
	$t = explode(":", $t);
	$dt = jmktime($t[0],$t[1],$t[2],$d[1],$d[2],$d[0]);
	$dt = date("Y-m-d H:i:s",$dt);
	if($lang == 'english')
		return $dt;
	else
		return tr_num($dt,'fa');
}
?>