<?php
//Security Check
require_once 'admin/main.php';
//Check Admin Login
if($isLoggedIn===1)
{
	$isLoggedIn = 1;
	header('location:admin/');
}
require_once 'header.php';
$error = "";//Variable for Show Error
require_once 'includes/functions.php';//Include Public Functions
//Login Error message 
if(isset($_GET['error']))
{
	if($_GET['error'] == 1)
	{
		$error = Error("نام کاربری یا کلمه عبور وارد شده اشتباه است",0);//Error Username & Password
	}
	elseif($_GET['error'] == 2)
	{
		$error = Error("کد امنیتی وارد شده اشتباه است",0);//Error CAPTCHA
	}
}
?>
<script>
</script>
<?php
//Login Form
echo '
		<main role="main" class="container">
	  <div class="jumbotron">
	<form action="admin/login.php"  method="post" id="loginform">
		<center><img id="login_logo" src="images/logo.png"></center>
		نام کاربری:<br>
			<input type="text" name="username" autofocus class="form-control" required dir="ltr"><br>
		گذرواژه:<br>
			<input class="form-control" type="password" name="password" required dir="ltr"><br>
			';
{
	echo'
	کد امنیتی زیر را وارد کنید:<br><img src = "includes/captcha.php" alt=""><br>
	<input type="text" name="security_code"  class="form-control" required dir="ltr"><br>
	';
}
echo'
			<span style="color:red;">'.$error.'</span><br>
			<label><input type="checkbox" name="remember" value="1"> مرا به خاطر بسپار</label><br>
			<center><input class="btn btn-primary" type="submit" name="login" id="login" value="ورود به سیستم"></center><br>
			<center><input class="btn btn-dark" type="submit" name="r_site" value="بازگشت به سایت"></center>
	</form>
	<script>
	$("main.container").css({"width":"300px"})
	</script>
';
require_once 'footer.php';
?>